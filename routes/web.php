<?php
/** Dashboard Routes */


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');

// Supplier crud using ajax,datatable for showing data
    Route::get('supplier', 'SupplierController@index');
    Route::get('supplier/getdata', 'SupplierController@supplierApi')->name('supplier.api');
    Route::post('supplier', 'SupplierController@store')->name('store.supplier');
    Route::get('supplier/{supplier_id}', 'SupplierController@edit');
    Route::post('supplier/{supplier_id}', 'SupplierController@update');
    Route::get('supplier/delete/{supplier_id}', 'SupplierController@destroy');

//Category crud completed using server side datatable,modal and ajax.

    Route::get('category', 'CategoryController@index')->name('category.index');
    Route::get('category/getdata', 'CategoryController@categoryApi')->name('category.api');
    Route::post('category/postdata', 'CategoryController@postData')->name('category.postdata');
    Route::get('category/fetchdata', 'CategoryController@fetchData')->name('category.fetchdata');
    Route::get('category/removedata', 'CategoryController@removeData')->name('category.removedata');
    Route::get('category/massremove', 'CategoryController@massRemove')->name('category.massremove');

//medicineType crud completed using server side datatable,modal and ajax.

    Route::get('medicineType', 'MedicineTypeController@index')->name('medicineType.index');
    Route::get('medicineType/getdata', 'MedicineTypeController@medicineTypeApi')->name('medicineType.api');
    Route::post('medicineType/postdata', 'MedicineTypeController@postData')->name('medicineType.postdata');
    Route::get('medicineType/fetchdata', 'MedicineTypeController@fetchData')->name('medicineType.fetchdata');
    Route::get('medicineType/removedata', 'MedicineTypeController@removeData')->name('medicineType.removedata');
    Route::get('medicineType/massremove', 'MedicineTypeController@massRemove')->name('medicineType.massremove');

//medicineUnit crud completed using server side datatable,modal and ajax.

    Route::get('medicineUnit', 'MedicineUnitController@index')->name('medicineUnit.index');
    Route::get('medicineUnit/getdata', 'MedicineUnitController@medicineUnitApi')->name('medicineUnit.api');
    Route::post('medicineUnit/postdata', 'MedicineUnitController@postData')->name('medicineUnit.postdata');
    Route::get('medicineUnit/fetchdata', 'MedicineUnitController@fetchData')->name('medicineUnit.fetchdata');
    Route::get('medicineUnit/removedata', 'MedicineUnitController@removeData')->name('medicineUnit.removedata');
    Route::get('medicineUnit/massremove', 'MedicineUnitController@massRemove')->name('medicineUnit.massremove');


//Shelf crud same as  category
    Route::get('/shelf', 'ShelfController@index');
    Route::post('dynamic-field/insert', 'ShelfController@insert')->name('dynamic-field.insert');
    Route::get('shelf/getdata', 'ShelfController@shelfApi')->name('shelf.api');
//end multiple row insert and show
    Route::get('shelf/fetchdata', 'ShelfController@fetchData')->name('shelf.fetchdata');
    Route::post('shelf/postdata', 'ShelfController@postData')->name('shelf.postdata');
    Route::get('shelf/removedata', 'ShelfController@removeData')->name('shelf.removedata');


//Customer lara crud
    Route::get('customer', 'CustomerController@index')->name('customer.index');
    Route::post('customer/store', 'CustomerController@storeData')->name('customer.storedata');
    Route::get('customer/{customer_id}', 'CustomerController@editData')->name('customer.editdata');
    Route::put('customer/{customer_id}', 'CustomerController@updateData')->name('customer.updatedata');
    Route::Delete('customer/{customer_id}', 'CustomerController@deleteData')->name('customer.deletedata');
    Route::get('deletecustomer/{customer_id}', 'CustomerController@deleteData')->name('customer.deletedata');

//Customer Search
    Route::get('/search/action', 'CustomerController@searchData')->name('search.action');

//Medicine
    Route::get('/medicine/index', 'MedicineController@index')->name('medicine.index');
    Route::get('medicine/create', 'MedicineController@createData')->name('medicine.createdata');
    Route::get('medicine/{medicine_id}', 'MedicineController@editData')->name('medicine.editdata');
    Route::post('medicine/store', 'MedicineController@storeData')->name('medicine.storedata');
    Route::put('medicine/{medicine_id}', 'MedicineController@updateData')->name('medicine.updatedata');
    Route::Delete('medicine/{medicine_id}', 'MedicineController@deleteData')->name('medicine.deletedata');

//Purchase
    Route::get('purchase/index', 'PurchaseController@index')->name('purchase.index');
    Route::get('purchase/create', 'PurchaseController@createData')->name('purchase.createdata');
    Route::get('select2_supplier_id', 'PurchaseController@getMedicineName')->name('select2_supplier_id');
    Route::get('purchase/select2_mediicne_id', 'PurchaseController@getMedicineData')->name('purchase.select2_medicine_id');
    Route::get('purchase/show/{purchase_id}', 'PurchaseController@showData')->name('purchase.showdata');
    Route::get('purchase/edit/{purchase_id}', 'PurchaseController@editData')->name('purchase.editdata');
    Route::post('purchase/store', 'PurchaseController@storeData')->name('purchase.storedata');
    Route::put('purchase/{purchase_id}', 'PurchaseController@updateData')->name('purchase.updatedata');
    Route::Delete('purchase/{purchase_id}', 'PurchaseController@deletePurchaseData')->name('purchase.deletedata');
    Route::Delete('purchase/product/{purchase_id}', 'PurchaseController@deletePurchaseProductData')->name('purchase.product.deletedata');
    Route::get('purchase/purchase_info/{purchase_id}', 'PurchaseController@editPurchaseInfoData')->name('purchase.purchase_info.editdata');
    Route::put('purchase/purchase_info/{purchase_id}', 'PurchaseController@purchaseInfoUpdateData')->name('purchase_info.updatedata');

//pos
    Route::get('pos/index', 'PosController@index')->name('pos.index');
    Route::get('pos/create', 'PosController@createData')->name('pos.createdata');
//Route::get('pos/select2_mediicne_id', 'PosController@getMedicineData')->name('pos.select2_medicine_id');
    Route::get('pos/select2_get_batch_id', 'PosController@getBatchId')->name('pos.select2_get_batch_id');
    Route::get('pos/select2_get_batch_data', 'PosController@getBatchData')->name('pos.select2_get_batch_data');

    Route::post('pos/store', 'PosController@storeData')->name('pos.storedata');
//p1
    Route::get('pos/show/{pos_id}', 'PosController@showPosData')->name('pos.showdata');
    Route::get('pos/invoice/{pos_id}', 'PosController@PosInvoiceData')->name('pos.invoicedata');
    Route::get('pos/edit/{pos_id}', 'PosController@editPosInfoData')->name('pos.editdata');
    Route::put('pos/update/{pos_id}', 'PosController@updatePosInfoData')->name('pos.updatedata');
    Route::Delete('pos/delete/{pos_id}', 'PosController@deletePosData')->name('pos.deletedata');
//p2
    Route::get('pos/product/{product_id}', 'PosController@editProductData')->name('pos.product.editdata');
    Route::Delete('pos/product/{product_id}', 'PosController@deleteProductData')->name('pos.product.deletedata');
    Route::put('pos/product/{product_id}', 'PosController@updateProductData')->name('pos.product.updatedata');

//Return
    Route::get('return/stock', 'ReturnProductController@index')->name('return.stock');
    Route::get('return/stock_list', 'ReturnProductController@returnList')->name('return.stock_list');
    Route::get('return/sold/product/{id}', 'ReturnProductController@returnSoldProduct')->name('return.sold.product');
    Route::post('store/return/product', 'ReturnProductController@storeReturnProduct');


//Report

    Route::get('report/index', 'ReportController@index')->name('report.index');
    Route::get('report/sales_report', 'ReportController@salesReport')->name('sales.report.index');
    Route::post('report/store/sales_report', 'ReportController@salesReport')->name('sales.report');
    Route::get('report/purchase_report', 'ReportController@purchaseReport')->name('purchase.report.index');
    Route::post('report/store/purchase_report', 'ReportController@purchaseReport')->name('purchase.report');
    Route::get('report/stock_out', 'ReportController@stockOutReport')->name('stock_out.report');
    Route::get('report/product_expired', 'ReportController@expiredProduct')->name('product_expired.report');
    Route::get('report/batch_wise', 'ReportController@stockBatchWise')->name('product_batch_wise_stock.report');

//Settings
    Route::get('settings/index', 'SettingController@index')->name('setting.index');
    Route::get('settings/edit/{id}', 'SettingController@editData')->name('setting.editdata');
    Route::put('settings/update/{id}', 'SettingController@updateData')->name('setting.updatedata');
    Route::Delete('settings/delete/{id}', 'SettingController@deleteData')->name('setting.deletedata');


});
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

