<!-- Brand Logo -->
<a href="{{ url('/') }}" class="brand-link">
    <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE</span>
</a>

<!-- Sidebar -->
<div class="sidebar nano">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="" class="d-block">{{ ucfirst(Auth::user()->name) }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->

            <li class="nav-item {{ isActive('/') }}">
                <a href="{{ action('DashboardController@index') }}" class="nav-link {{ isActive('/') }}">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Dashboard

                    </p>
                </a>
            </li>

            <li class="nav-item has-treeview {{ isActive(['supplier*']) }}">
                <a href="#" class="nav-link {{ isActive(['supplier*']) }}">
                    <i class="nav-icon fas fa fa-users"></i>
                    <p>
                        Supplier
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ action('SupplierController@index') }}"
                           class="nav-link {{ isActive('supplier') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Supplier Data</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview {{ isActive(['category*']) }}">
                <a href="#" class="nav-link {{ isActive(['category*']) }}">
                    <i class="nav-icon fa fa-bolt"></i>
                    <p>
                        Category
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('category.index')}}"
                           class="nav-link {{ isActive('category') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Category Data</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview {{ isActive(['medicineType*']) }}">
                <a href="#" class="nav-link {{ isActive(['medicineType*']) }}">
                    <i class="nav-icon fa fa-bolt"></i>
                    <p>
                        Medicine Type
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('medicineType.index')}}"
                           class="nav-link {{ isActive('medicineType') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Medicine Type Data</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview {{ isActive(['medicineUnit*']) }}">
                <a href="#" class="nav-link {{ isActive(['medicineUnit*']) }}">
                    <i class="nav-icon fa fa-bolt"></i>
                    <p>
                        Medicine Unit
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('medicineUnit.index')}}"
                           class="nav-link {{ isActive('medicineUnit') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Medicine Unit Data</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview {{ isActive(['shelf*']) }}">
                <a href="#" class="nav-link {{ isActive(['shelf*']) }}">
                    <i class="nav-icon fas fa fa-th"></i>
                    <p>
                        Shelf
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ action('ShelfController@index') }}"
                           class="nav-link {{ isActive('shelf*') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Shelf Data</p>
                        </a>
                    </li>

                </ul>
            </li>
            @can('isAdmin')
            <li class="nav-item has-treeview {{ isActive(['medicine*']) }}">
                <a href="#" class="nav-link {{ isActive(['medicine*']) }}">
                    <i class="nav-icon fas fa fa-medkit"></i>
                    <p>
                        Medicine
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('medicine.index')}}"
                           class="nav-link {{ isActive('medicine/index') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Medicine List</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('medicine.createdata') }}"
                           class="nav-link {{ isActive('medicine/create') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Add Medicine</p>
                        </a>
                    </li>

                </ul>

            </li>
            @endcan
            <li class="nav-item has-treeview {{ isActive(['customer*']) }}">
                <a href="#" class="nav-link {{ isActive(['customer*']) }}">
                    <i class="nav-icon  fa fa-leaf"></i>
                    <p>
                        Customer
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('customer.index')}}"
                           class="nav-link {{ isActive('customer') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Customer Data</p>
                        </a>
                    </li>

                </ul>
            </li>
        @can('isAdmin')
            <li class="nav-item has-treeview {{ isActive(['purchase*']) }}">
                <a href="{{action('PurchaseController@index')}}" class="nav-link {{ isActive(['purchase*']) }}">
                    <i class="nav-icon fas fa fa-bars"></i>
                    <p>
                        Purchase
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{action('PurchaseController@createData')}}"
                           class="nav-link {{ isActive('purchase/create') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Add Purchase</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('purchase/index')}}"
                           class="nav-link {{ isActive('purchase/index') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Manage Purchase</p>
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            <li class="nav-item has-treeview {{ isActive(['pos*']) }}">
                <a href="{{url('pos/index')}}" class="nav-link {{ isActive(['pos*']) }}">
                    <i class="nav-icon fas fa fa-calculator"></i>
                    <p>
                        Invoice
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('pos/create')}}"
                           class="nav-link {{ isActive('pos/create') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>New Bill</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('pos/index')}}"
                           class="nav-link {{ isActive('pos/index') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Manage Invoice</p>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item has-treeview {{ isActive(['return*']) }}">
                <a href="#" class="nav-link {{ isActive(['return*']) }}">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                        Return
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('return/stock')}}" class="nav-link {{ isActive('return/stock') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Stock Return</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('return/stock_list')}}" class="nav-link {{ isActive('return/stock_list') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Stock Return List</p>
                        </a>
                    </li>

                </ul>


            </li>
            <li class="nav-item has-treeview {{ isActive(['report*']) }}">
                <a href="#" class="nav-link {{ isActive(['report*']) }}">
                    <i class="nav-icon fas fa fa-book"></i>
                    <p>
                        Report
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/index')}}"
                           class="nav-link {{ isActive('report/index') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Stock</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/batch_wise')}}"
                           class="nav-link {{ isActive('report/batch_wise') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Stock(Batch Wise)</p>
                        </a>
                    </li>

                </ul>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/sales_report')}}" class="nav-link {{ isActive('report/sales_report') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Sales Report</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/purchase_report')}}"
                           class="nav-link {{ isActive('report/purchase_report') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Purchase Report</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/stock_out')}}" class="nav-link {{ isActive('report/stock_out') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Stock Out Report</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('report/product_expired')}}"
                           class="nav-link {{ isActive('report/product_expired') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Expired Medicine</p>
                        </a>
                    </li>

                </ul>

            </li>
            <li class="nav-item has-treeview {{ isActive(['settings*']) }}">
                <a href="#" class="nav-link {{ isActive(['settings*']) }}">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                        Settings
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('settings/index')}}" class="nav-link {{ isActive('settings/index') }}">
                            <i class="fa fa-check nav-icon"></i>
                            <p>Settings</p>
                        </a>
                    </li>

                </ul>


                </ul>

            </li>


        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
