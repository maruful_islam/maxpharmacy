<!-- Left navbar links -->
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
</ul>
<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown" style="margin-top:8px;">

        <a href="{{ url('/logout') }}"> logout </a>
    </li>


    <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                    class="fas fa-th-large"></i></a>
    </li>
</ul>