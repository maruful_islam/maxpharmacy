<div class="modal-header">
    <h4 class="modal-title">Add Category</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">

    <span id="form_output"></span>
    <div class="form-group">
        {!! Form::label('name','Category Name:', ['class' => 'control-label']) !!}
        {!! Form::text('name',null,['class' => 'form-control','placeholder'=>'Category Name']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description','Category Description',['class'=>'control-label']) !!}
        {!! Form::text('description',null,['class'=>'form-control','placeholder'=>"Category Description"]) !!}
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" name="category_id" id="category_id" value="" />
    <input type="hidden" name="button_action" id="button_action"
           value="insert"/>
    <input type="submit" name="submit" id="action" value="Add"
           class="btn btn-primary"/>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close
    </button>
</div>
