@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{ $message }}</strong></div>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert‐danger alert‐dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{ $message }}</strong></div> @endif
@if ($message = Session::get('warning'))
    <div class="alert alert‐warning alert‐dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{ $message }}</strong></div> @endif
@if ($message = Session::get('info'))
    <div class="alert alert‐info alert‐dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{ $message }}</strong></div> @endif

