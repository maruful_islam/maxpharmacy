<div class="card-body">
    {!! Form::hidden('id','',['id'=>'id']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Supplier Name:', ['class' => 'control-label']) !!}
        {!! Form::text('name','', ['class' => 'form-control','placeholder'=>'Supplier Name']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Email Address',['class'=>'control-label']) !!}
        {!! Form::email('email','',['class'=>'form-control','placeholder'=>"Enter Email"]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('phone','Phone Number',['class'=>'control-label']) !!}
        {!! Form::text('phone','',['class'=>'form-control','placeholder'=>"Enter Phone Number"]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('details','Details',['class'=>'control-label']) !!}
        {!! Form::textarea('details','',['class'=>'form-control','rows'=>3]) !!}

{{--            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>--}}

        <div class="form-group">
            {!! Form::label('logo','Enter Logo',['class'=>'control-label']) !!}
            <div class="input-group">
                <div class="custom-file">

                    {!!  Form::file('logo') !!}

                </div>
                <div class="input-group-append">
                    <span class="input-group-text" id="">Upload</span>

                </div>

            </div>
            <img id="user_uploaded_image" style="height: 80px;width:80px">
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary','id'=>'btn_ctrl']) !!}
    </div>
</div>
