@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')
@section('style')
    <style type="text/css">
        #image_preview { max-width: 200px; max-height: 200px; display: block; }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Medicine</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Medicine List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">

                            <h3 class="card-title">Update Medicine</h3>
                        </div>

                        {!! Form::model($medicine, ['method' => 'PUT','route' => ['medicine.editdata', $medicine->id],'enctype'=>'multipart/form-data']) !!}
                        @include('admin.medicine.form')
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('medicine.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                        {!! Form::close() !!}

                    </div>


                </div>



            </div>
        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">
 </script>
@stop
