<div class="max-s1" style="padding: 1%">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">
                {{ Form::label('Supplier:')}}
                {{Form::select("supplier_id",$supplier_data,null,
                 [
                    "class" => "form-control",
                    "placeholder" => "Select Supplier"
                 ])
    }}
                <span class="text-danger">{{ $errors->first('supplier_id') }}</span>
            </div>
            <div class="form-group ">
                {{ Form::label('Medicine Name:') }}
                {{ Form::text('medicine_name',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('medicine_name') }}</span>

            </div>
            <div class="form-group  ">
                {{ Form::label('Medicine Type:')}}
                {{Form::select("medicine_type",$repository->types(),null,
                 [
                    "class" => "form-control",
                    "placeholder" => "Select Type"
                 ])
    }}

                <span class="text-danger">{{ $errors->first('medicine_type') }}</span>

            </div>

            <div class="form-group">
                {{ Form::label('Medicine Details:') }}
                {{ Form::text('medicine_details',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('medicine_details') }}</span>
            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group  ">
                {{ Form::label('Category:')}}
                {{Form::select("category_id",$category_data, null,
                 [
                    "class" => "form-control",
                    "placeholder" => "Select Category"
                 ])

    }}
                <span class="text-danger">{{ $errors->first('category_id') }}</span>
            </div>
            <div class="form-group  ">
                {{ Form::label('Generic Name:') }}
                {{ Form::text('generic_name',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('generic_name') }}</span>
            </div>
            <div class="form-group  ">
                {{ Form::label('Medicine Unit:')}}
                {{Form::select("unit",$repository->units(),null,
                 [
                    "class" => "form-control",
                    "placeholder" => "Select Unit"
                 ])
    }}
                <span class="text-danger">{{ $errors->first('unit') }}</span>
            </div>
            <div class="form-group  ">
                {{ Form::label('Selling Price(Unit):') }}
                {{ Form::text('selling_price',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('selling_price') }}</span>
            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group  ">
                {{ Form::label('Shelf Number')}}
                {{Form::select("shelf_id",$shelf_data, null,
                 [
                    "class" => "form-control",

                 ])
    }}
                <span class="text-danger">{{ $errors->first('shelf_id') }}</span>
            </div>

            <div class="form-group  ">
                {{ Form::label('Pack Size:') }}
                {{ Form::text('box_size',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('box_size') }}</span>
            </div>

            <div class="form-group  ">
                {{ Form::label('Supplier Price(Unit):') }}
                {{ Form::text('supplier_price',null,['class'=>'form-control'])}}
                <span class="text-danger">{{ $errors->first('supplier_price') }}</span>
            </div>

            <div class="form-group  ">

                {{ Form::label('Image:') }}
                {{ Form::file('medicine_image',['class'=>'form-control','id'=>'file_to_upload'])}}
            </div>
            <img id="image_preview" src="{{(isset($medicine))?asset('admin/upload/'.$medicine->medicine_image):''}}"
                 alt="Preview Image">

        </div>


    </div>
</div>


