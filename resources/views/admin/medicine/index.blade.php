@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Medicine</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Medicine</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div class="card-header">

                            <h3 class="card-title">Medicine List</h3>
                        </div>
                        <table id="supplier" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Medicine Image</th>
                                <th>Medicine Name</th>
                                <th>Type</th>
                                <th>Details</th>
                                <th>Category</th>
                                <th>Supplier</th>
                                <th>Shelf</th>
                                <th>Generic Name</th>
                                <th>Unit</th>
                                <th>Pack Size</th>
                                <th>Selling Price(unit)</th>
                                <th>Supplier Price(unit)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_data as $single_data)
                                <tr>
                                    <td>{{$single_data->id}}</td>
                                    <td>
                                        @if($single_data->medicine_image)
                                            <img src="{{asset('admin/upload/'.$single_data->medicine_image)}}" alt=""
                                                 style="width: 80px;height: 80px">
                                        @endif

                                    </td>
                                    <td>{{$single_data->medicine_name}}</td>
                                    <td>{{$single_data->medicine_type}}</td>
                                    <td>{{$single_data->medicine_details}}</td>
                                    <td>{{$single_data->category->name}}</td>
                                    <td>{{$single_data->supplier->name}}</td>
                                    <td>{{$single_data->shelf->shelf_number}}</td>
                                    <td>{{$single_data->generic_name}}</td>
                                    <td>{{$single_data->unit}}</td>
                                    <td>{{$single_data->box_size}}</td>
                                    <td>{{$single_data->selling_price}}</td>
                                    <td>{{$single_data->supplier_price}}</td>
                                    <td>


                                        <a class="btn btn-xs  btn-primary"
                                           href="{{route('medicine.editdata',$single_data->id)}}"><i class="fas fa-edit"></i></a>

                                        {!! Form::open(['method' => 'DELETE','route' => ['medicine.deletedata', $single_data->id],'style'=>'display:inline']) !!}
                                        {!! Form::button('<i class="fa fa-minus-circle" aria-hidden="true"></i>',
                                        ['class' => 'btn btn-xs btn-warning','type' => 'submit','onClick'=>'return confirm("are you sure to delete?")'])
                                         !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {{$all_data->render()}}

                    </div>


                </div>


            </div>
        </div>
    </section>

@stop


@section('script')
    <script type="text/javascript">

    </script>
@stop
