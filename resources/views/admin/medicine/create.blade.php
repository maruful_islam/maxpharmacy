@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('style')
    <style type="text/css">
        #image_preview { max-width: 200px; max-height: 200px; display: block; }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Medicine</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Medicine</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div class="card-header">

                            <h3 class="card-title">Add Medicine</h3>
                        </div>
                        <!-- form start -->


                        {!! Form::open(['id'=>'medicine_form','method'=>'post','files'=>true,'url'=>route('medicine.storedata')])  !!}
                        @include('admin.medicine.form')
                        <div class="card-footer">
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary','id'=>'btn_ctrl']) !!}
                        </div>
                        {{ Form::close() }}

                    </div>


                </div>


            </div>
        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#file_to_upload").on("change", function () {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return;

                if (/^image/.test(files[0].type)) { // Allow only image upload
                    var ReaderObj = new FileReader(); // Create instance of the FileReader
                    ReaderObj.readAsDataURL(files[0]); // read the file uploaded
                    ReaderObj.onloadend = function () {
                        $("#image_preview").attr('src', this.result);
                    }
                } else {
                    alert("Upload an image");
                }
            });
        });

    </script>
@stop
