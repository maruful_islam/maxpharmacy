@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }

        .initiallyHidden {
            display: none;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Point of Sale</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Pos</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <span id="result"></span>
                        <div class="card-header">

                            <h3 class="card-title">Add Sale</h3>
                        </div>


                        {!! Form::open(['id'=>'dynamic_form'])  !!}
                        @include('admin.pos.form')
                        {{ Form::close() }}

                    </div>


                </div>


            </div>
        </div>
    </section>
    {{--@dd($repository->medicines()))--}}
@stop
@section('script')
    <script type="text/javascript">


        $(document).ready(function () {

            var count = 1;

            dynamic_field(count);

            function dynamic_field(count) {

                html = '<tr>';
                html += '<td> {{Form::select("medicine_id[]",$repository->medicines(),null,
         [
            "class" => "form-control select2 sale_medicine_class",
            "placeholder" => "Select Medicine",
            "id"=>"medicine_id",'style'=>'width:140px'
         ])
}}  </td>';


                html += '<td> <select class="form-control  batch_class select2" name="batch_id[]" id="batch_id" style="width: 140px;"><option value=""></option></select></td>';


                html += '<td><input type="text" name="stock_status[]" class="form-control stock_status" id="stock_status" placeholder="Available" readonly/></td>';
                html += '<td><input type="text" name="expire_date[]" class="form-control expire_date" id="expire_date"  readonly/></td>';

                html += '<td><input type="text" name="medicine_type[]" class="form-control medicine_type" placeholder="Types" id="medicine_type" readonly/></td>';
                html += '<td><input type="text" name="purchase_unit[]" class="form-control medicine_unit" id="medicine_unit" placeholder="Unit"   readonly/></td>';
                html += '<td><input type="text" name="box_size[]" class="form-control box_size" id="box_size" placeholder="Pack Size"  readonly/></td>';
                html += '<td><input type="text" name="unit_price[]" class="form-control unit_price" id="unit_price" placeholder="Unit Price" /></td>';
                html += '<td><input type="text" name="sale_quantity[]" class="form-control sale_quantity" id="purchase_quantity" placeholder="Quantity" /></td>';
                html += '<td><input type="text" name="sale_total_amount[]" class="form-control sale_total" id="purchase_total" placeholder="Total" readonly/></td>';
                if (count > 1) {
                    html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                    $('#user_table').append(html);
                } else {
                    html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                    $('#user_table').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);


            });

            $(document).on('click', '.remove', function () {
                count--;
                $(this).closest("tr").remove();
            });

//            $(document).on('change', '.sale_medicine_class', function () {
//                id = $(this).val();
//                var className = Math.floor(Math.random() * 1000).toString();
//                $(this).parents('tr').addClass(className);
//                appendMedicineSingleData(id, className);
//
//
//            });
            $(document).on('change', '.sale_medicine_class', function () {
                id = $(this).val();
                $.ajax({
                    url: "{{route('pos.select2_get_batch_id')}}",
                    method: 'get',
                    data: {id: id},
                    success: function (data) {
                        $.each(data, function (key, value) {
                            $('.batch_class').append('<option value="' + value + '">' + value + '<option>');
                        })
                    }
                })
            });

            $(document).on('change', '.batch_class', function () {
                var id = $(this).val();

                var className = Math.floor(Math.random() * 1000).toString();
                $(this).parents('tr').addClass(className);
                appendMedicineSingleData(id, className);


            });


            function appendMedicineSingleData(id, className) {


                var medicine_id = $('.' + className).find('.sale_medicine_class').val();
                // var batch_id = $('.' + className).find('.batch_class').val();
                alert(id);
                $.ajax({
                    url: "{{ route('pos.select2_get_batch_data') }}",
                    method: 'GET',
                    data: {
                        id: id,
                        medicine_id: medicine_id,
                    },

                    success: function (data) {

                        // var expire_date = data.expire_date;
                        var stock_avail = data.stock_avail;
                        var expire_date = data.expire_date;
                        console.log('check ex', stock_avail);

                        if (stock_avail < 0) {
                            alert(" Stock Is not Available");
                            $('.' + className).remove();
                        }
                         else if (new Date() > new Date(expire_date)) {
                             alert(" Medicine Expired");
                             $('.' + className).remove();
                         }
                        else {
                            $('.' + className).find('.stock_status').val(stock_avail);
                            $('.' + className).find('.medicine_type').val(data[0]['medicine_type']);
                             $('.' + className).find('.expire_date').val(expire_date);
                            $('.' + className).find('.medicine_unit').val(data[0]['unit']);
                            $('.' + className).find('.box_size').val(data[0]['box_size']);
                            $('.' + className).find('.unit_price').val(data[0]['selling_price']);

                        }
                        $(document).on('keyup', '.sale_quantity', function () {
                            var cl = $(this).parents('tr').attr('class');
                            var pack_size = $('.' + cl).find('.box_size').val();
                            console.log(pack_size);
                            var unit_price = $('.' + cl).find('.unit_price').val();
                            var quantity = $('.' + cl).find('.sale_quantity').val();
                            var total_amount = parseFloat(pack_size) * parseFloat(quantity) * parseFloat(unit_price);
                            $('.' + cl).find('.sale_total').val(total_amount.toFixed(2));


                        });


                        $(document).on('keyup', '.sale_invoice_discount', function () {
                            var grand_total = 0;
                            $('.sale_total').each(function () {
                                grand_total += Number($(this).val());
                            });
                            $('.sale_sub_total').val(grand_total.toFixed(2));
                            var sale_invoice_discount = $('.sale_invoice_discount').val();
                            var temp_total = parseFloat(grand_total) - parseFloat(sale_invoice_discount);
                            $('.sale_grand_total').val(temp_total.toFixed(2));
                        });
                        $(document).on('keyup', '.sale_paid_amount', function () {


                            var grand_total_final = $('.sale_grand_total').val();
                            var paid_amount = $('.sale_paid_amount').val();
                            var due_total = parseInt(grand_total_final) - parseInt(paid_amount);
                            $('.sale_due_amount').val(due_total.toFixed(2));

                        });
                    }
                })
            }


            $('#dynamic_form').on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: '{{ route("pos.storedata") }}',
                    method: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.error) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<p>' + data.error[count] + '</p>';
                            }
                            $('#result').html('<div class="alert alert-danger">' + error_html + '</div>');
                        } else {
                            dynamic_field(1);
                            window.location.href = "{{url('pos/invoice')}}" + '/' + data.sale_id;
                            $('#result').html('<div class="alert alert-success">' + data.success + '</div>');
                        }

                    }
                })
            });

            $('.select2').select2();

        })
        ;


        $(".customer_add").click(function () {
            $(".ctrl").toggle();
        });


    </script>
@stop
