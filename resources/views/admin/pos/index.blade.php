@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Sales</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All Sale List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-header" style="margin-bottom: 1%">
                            <h3 class="card-title">All Sale List</h3>
                        </div>
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th> Date</th>
                                <th>Invoice No</th>
                                <th>Customer</th>
                                <th>Total Amount</th>
                                <th>Paid Amount</th>
                                <th>Discount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($allData as $data)
                                {{--@dd($data)--}}
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->sale_invoice_date}}</td>
                                    <td>{{$data->sale_invoice_no}}</td>
                                    <td>{{$data->customers->customer_name}}</td>


                                    <td>
                                        @if($data->saleDetails())
                                            {{$data->saleDetails()->sum('sale_total_amount')}}
                                        @endif
                                    </td>
                                    <td>
                                        {{$data->sale_paid_amount}}

                                    </td>

                                    <td>
                                        {{$data->sale_invoice_discount}}
                                    </td>
                                    <td>


                                        <a class="btn btn-xs  btn-outline-secondary"
                                           href="{{route('pos.invoicedata',$data->id)}}"><i class="fas fa-print"></i></a><a class="btn btn-xs  btn-outline-secondary"
                                           href="{{route('pos.showdata',$data->id)}}"><i class="fas fa-eye"></i></a>
                                        <a class="btn btn-xs btn-outline-secondary" href="{{route('pos.editdata',$data->id)}}"><i
                                                class="fas fa-pen"></i></a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['pos.deletedata', $data->id],'style'=>'display:inline']) !!}
                                        {!! Form::button('<i class="fa fa-minus-circle" aria-hidden="true"></i>',
                                        ['class' => 'btn btn-xs btn-outline-secondary','type' => 'submit','onClick'=>'return confirm("are you sure to delete?")'])
                                         !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop


@section('script')
    <script type="text/javascript">

        $('#myTable').DataTable({})

    </script>
@stop
