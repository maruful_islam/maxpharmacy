@extends('admin.layouts.fixed')

@section('title','Max Pharmacy | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Invoice Details</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Invoice Details</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <br>
    <br>
    <br>
    <br>
    <br>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <div class="left_col">
                        <h4>Invoice</h4>
                        <h6>Invoice No :<b>{{ $sale_data->sale_invoice_no }}</b></h6>
                        <h6>Billing Date : <b>{{ $sale_data->sale_invoice_date }}</b></h6>
                        <span style="color: #00a65a;font-size: large"><b>Billing To</b></span>
                        <h6><b>Name : </b>{{$sale_data->customers->customer_name}}</h6>
                        <h6><b>Address: </b>{{$sale_data->customers->details}} </h6>
                        <h6><b>Mobile: </b>{{$sale_data->customers->customer_phone_number}} </h6>
                        <h6><b>Mobile: </b>{{$sale_data->customers->email}} </h6>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1">

                    <div class="left_col">
                        @foreach($settings as $single_data)
                            <span style="color: #00a65a;font-size: large"><b>Billing From</b></span>
                            <h6><b>Name:</b>{{$single_data->pharmacy_name}}</h6>
                            <h6><b>Address:</b>{{$single_data->pharmacy_address}}</h6>
                            <h6><b>Mobile:</b>{{$single_data->pharmacy_mobile}}</h6>
                            <h6><b>Email:</b>{{$single_data->pharmacy_email}}</h6>
                            <h6><b>Website:</b>{{$single_data->pharmacy_website}}</h6>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <br>
    <br>

    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card" style="background-color: #f4f6f9">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Medicine Name</th>
                            <th>Quantity</th>
                            <th>Sale Price</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php $i=0; @endphp
                        @foreach($sale_details as $single_data)
                            @php $i++; @endphp
                            <tr>
                                <td style="background-color: #f4f6f9">{{$i}}</td>
                                <td>{{$single_data->medicine->medicine_name}}</td>
                                <td>{{$single_data->sale_quantity}}</td>
                                <td>{{$single_data->medicine->selling_price}}</td>
                                <td>{{$single_data->sale_total_amount}}</td>

                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"><b>Sub Total</b></td>
                            <td colspan="4"><b>{{$sale_data->saleDetails()->sum('sale_total_amount')}}</b></td>
                        </tr>
                        </tbody>

                    </table>
                    <div class="row">
                        <div class="col-md-4 offset-2">

                        </div>
                        <div class="col-md-4 offset-2">
                            <h6><b>Sub Total : </b>{{$sale_data->saleDetails()->sum('sale_total_amount')}}</h6>
                               <h6><b>Discount : </b>{{$sale_data->sale_invoice_discount}}</h6>
                               <h6><b>Grand Total : </b>{{$sale_data->saleDetails()->sum('sale_total_amount')-$sale_data->sale_invoice_discount}}</h6>
                            <h6><b>Paid Total : </b>{{$sale_data->sale_paid_amount}}</h6>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-4 offset-2">
                            ______________
                            <h4>Received By</h4>
                        </div>
                        <div class="col-md-4 offset-2">

                            ______________
                            <h4>Authorised By</h4>
                        </div>
                    </div>


                </div>


            </div>


        </div>
    </section>
    <section class="content-footer no-print">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 offset-2">
                    <a class="btn btn-xs  btn-info "
                       href="{{route('pos.index')}}"><i class="fas fa-backspace"></i></a>
                    <a class="btn btn-xs  btn-danger print-window" href=""><i
                                class="fas fa-print"></i></a>
                </div>
            </div>
        </div>
        </div>
    </section>


@stop


@section('script')
    <script type="text/javascript">

        $('.print-window').click(function () {
            window.print();
        });
    </script>
@stop
