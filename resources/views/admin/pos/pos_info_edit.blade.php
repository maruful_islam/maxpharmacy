@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')
@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }
    </style>
@stop
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{--<h1 style="text-align: center">Purchase Info Edit</h1>--}}
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Pos Info Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 offset-md-2">

                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div class="card-header">

                            <h3 class="card-title">Edit Pos Info</h3>
                        </div>
                        {!! Form::model($pos, ['method' => 'PUT','route' => ['pos.updatedata', $pos->id],'enctype'=>'multipart/form-data']) !!}
                        <div class="row" style="padding: 1%">
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >

                                    {{ Form::label('Customer Phone Number:')}}

                                    {{Form::select("customer_id",$repository->customers(),null,
                             [
                                "class" => "form-control select2 supplier_class",
                                "placeholder" => "Select Phone",
                                "id"=>"supplier_id"
                             ])
                    }}


                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                    {!! Form::label('Date', 'Invoice Date:', ['class' => 'control-label','value'=>"{{ old('sale_invoice_date') }}" ]) !!}
                                    {!! Form::text('sale_invoice_date', date('d/m/y'), ['class' => 'form-control','placeholder'=>'Date']) !!}
                                    <span class="text-danger">{{ $errors->first('sale_invoice_date') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                    {!! Form::label('Invoice No', 'Invoice No:', ['class' => 'control-label','value'=>"{{ old('sale_invoice_no') }}" ]) !!}
                                    {!! Form::text('sale_invoice_no', null, ['class' => 'form-control','placeholder'=>'Invoice No','readonly']) !!}
                                    <span class="text-danger">{{ $errors->first('sale_invoice_no') }}</span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('Total Amount:') }}
                                    {{ Form::text('sale_total_amount',$pos->saleDetails()->sum('sale_total_amount'),['class'=>'form-control sale_total_amount','readonly'])}}
                                    <span class="text-danger">{{ $errors->first('sale_total_amount') }}</span>

                                </div>
                            </div>
                            <div class="col-md-12">



                                <div class="form-group ">
                                    {{ Form::label('Discount:') }}
                                    {{ Form::text('sale_discount',null,['class'=>'form-control sale_discount'])}}
                                    <span class="text-danger">{{ $errors->first('sale_discount') }}</span>

                                </div>
                                <div class="form-group ">
                                    {{ Form::label('Paid Amount:') }}
                                    {{ Form::text('sale_paid_amount',null,['class'=>'form-control sale_paid_amount'])}}
                                    <span class="text-danger">{{ $errors->first('sale_paid_amount') }}</span>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('Due:') }}
                                    {{ Form::text('sale_due_amount',null,['class'=>'form-control sale_due_amount','readonly'])}}
                                    <span class="text-danger">{{ $errors->first('sale_due_amount') }}</span>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('sale Notes:') }}
                                    {{ Form::text('sale_notes',null,['class'=>'form-control sale_notes'])}}
                                    <span class="text-danger">{{ $errors->first('sale_notes') }}</span>

                                </div>
                            </div>

                            <div class="card-footer">
                                {!! Form::submit('Update', ['class' => 'btn btn-primary','id'=>'btn_ctrl']) !!}
                                <a href="{{route('pos.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">
        $(document).on('keyup', '.sale_paid_amount', function () {

            var total_amount = $('.sale_total_amount').val();
            var paid = $('.sale_paid_amount').val();
            var discount = $('.sale_discount').val();
            var due = parseInt(total_amount) - (parseInt(paid)+parseInt(discount));
            $('.sale_due_amount').val(due);
        });

    </script>
@stop
