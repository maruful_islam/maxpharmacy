<div class="max-s1" style="padding: 1%">
    <div class="row">
        <span id="result"></span>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('Date', 'Date:', ['class' => 'control-label','value'=>"{{ old('pos_invoice_date') }}" ]) !!}
                {!! Form::text('sale_invoice_date', date('d/m/Y'), ['class' => 'form-control','placeholder'=>'Date']) !!}

            </div>
        </div>
        <div class="col-md-4 offset-md-2">

            <div class="form-group">
                {!! Form::label('Customer Phone Number', 'Customer Phone Number:', ['class' => 'control-label','value'=>"{{ old('sale_invoice_date') }}" ]) !!}
                {{Form::select("customer_id",$repository->customers(),null,
                [
                   "class" => "form-control select2 customer_class",
                   "placeholder" => "Select Phone",
                   "id"=>"customer_id"
                ])
       }}

            </div>

        </div>
        <div class="col-md-2 ">
            {!! Form::label('', 'Add New Customer:' ) !!}
                        <a class="btn btn-info" href="#">
                        <i class="icon-plus icon-large customer_add"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="custom col-md-6">

        </div>

        <div class="col-md-2">
            <div class="form-group ctrl initiallyHidden">
                {!! Form::text('customer_name', null, ['class' => 'form-control customer_name','id'=>'customer_name','placeholder'=>'Customer Name']) !!}

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group ctrl initiallyHidden">
                {!! Form::text('customer_phone_number', null, ['class' => 'form-control customer_phone_number','id'=>'customer_phone_number','placeholder'=>'Customer Phone Number']) !!}


            </div>
        </div>


    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr style="text-align: center;">
            <th style="width:10%">Medicine</th>
            <th style="width:10%">Batch</th>
            <th style="width:9%">Stock Quantity</th>
            <th style="width:9%">Expire Date</th>
            <th style="width:9%">Type</th>
            <th style="width:9%">Unit</th>
            <th style="width:9%">Pack Size</th>
            <th style="width:9%">Unit Price</th>
            <th style="width:9%">Quantity</th>
            <th style="width:9%">Total</th>
            <th style="width:4%">Action</th>

        </tr>
        </thead>
        <tbody>

        </tbody>
        <tfoot>


        </tfoot>
    </table>
    <table class="table table-bordered table-striped test" id="user_table">

    </table>
    <div class="row">
        <div class="custom col-md-10">

            <div class="custom col-md-10" style="margin-bottom: -5%">
                <div class="form-group">

                    {!! Form::textarea('sale_notes', null, ['class' => 'form-control','placeholder'=>'SaleNotes','style'=>'height:60px']) !!}

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">


            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">

        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::text('sale_sub_total', null, ['class' => 'form-control sale_sub_total','placeholder'=>'Sub Total','readonly']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::text('sale_invoice_discount', null, ['class' => 'form-control sale_invoice_discount','placeholder'=>'Invoice Discount']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::text('sale_grand_total', null, ['class' => 'form-control sale_grand_total','placeholder'=>'Grand Total','readonly']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::text('sale_paid_amount', null, ['class' => 'form-control sale_paid_amount','placeholder'=>'Paid Amount','id'=>'pos_paid_amount']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::text('sale_due_amount', null, ['class' => 'form-control sale_due_amount','id'=>'pos_due_amount','placeholder'=>'Due','readonly']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            {{--<div class="form-group">--}}
            {{--{!! Form::text('pos_change', null, ['class' => 'form-control pos_change','placeholder'=>'Change','id'=>'pos_change','readonly']) !!}--}}

        </div>
    </div>


    {{--</div>--}}
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            {!! Form::submit('submit', ['class' => 'btn btn-primary btn-lg','id'=>'save']) !!}
        </div>


    </div>
</div>



