@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')
@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Sale Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Product List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">

                            <h3 class="card-title">Update Sale Product Details</h3>
                        </div>
                        {!! Form::model($pos_product_details, ['method' => 'PUT','route' => ['pos.product.updatedata', $pos_product_details->id],'enctype'=>'multipart/form-data']) !!}
                        <div class="max-s1" style="padding: 1%">
                            <div class="row">
                                <span id="result"></span>
                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Medicine Name', 'Medicine Name:', ['class' => 'control-label','value'=>"{{ old('medicine_name') }}" ]) !!}


                                        {!! Form::text('medicine_name',$pos_product_details->medicine->medicine_name, ['class' => 'form-control','placeholder'=>'','readonly']) !!}

                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Medicine Type','Medicine Type:', ['class' => 'control-label','value'=>"{{ old('medicine_type') }}" ]) !!}
                                        {!! Form::text('medicine_type',$pos_product_details->medicine->medicine_type, ['class' => 'form-control medicine_type','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Pack Size', 'Pack Size', ['class' => 'control-label ','value'=>"{{ old('box_size') }}" ]) !!}
                                        {!! Form::text('box_size',$pos_product_details->medicine->box_size, ['class' => 'form-control box_size ','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Unit', 'Unit', ['class' => 'control-label ','value'=>"{{ old('purchase_unit') }}" ]) !!}
                                        {!! Form::text('unit', $pos_product_details->medicine->unit, ['class' => 'form-control purchase_unit','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Unit Price', 'Unit Price', ['class' => 'control-label ','value'=>"{{ old('unit_price') }}" ]) !!}
                                        {!! Form::text('unit_price', $pos_product_details->medicine->selling_price, ['class' => 'form-control unit_price','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Quantity', 'Quantity', ['class' => 'control-label','value'=>"{{ old('sale_quantity') }}" ]) !!}
                                        {!! Form::text('sale_quantity', null, ['class' => 'form-control sale_quantity','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('sale_quantity') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >


                                        {!! Form::label('Total', 'Total', ['class' => 'control-label ','value'=>"{{ old('sale_total_amount') }}" ]) !!}
                                        {!! Form::text('sale_total_amount', null, ['class' => 'form-control sale_total_amount','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('sale_total_amount') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >


                                        {!! Form::hidden('sale_id',null, ['class' => 'form-control','placeholder'=>'']) !!}

                                    </div>
                                </div>


                            </div>

                        </div>

                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('pos.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                        {!! Form::close() !!}

                    </div>


                </div>


            </div>

        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">

        $(document).on('keyup', '.sale_quantity', function () {

            var quantity = $('.sale_quantity').val();
            var box_size = $('.box_size').val();
            var unit_price = $('.unit_price').val();
            var change_total = unit_price * box_size * quantity;
            $('.sale_total_amount').val(change_total);
            console.log('total', change_total);
            console.log('Grand total', change);


        });
    </script>
@stop
