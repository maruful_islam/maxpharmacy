@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Medicine Type')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Medicine Type</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Medicine Type</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div id="medicineTypeModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    {!! Form::open(['id'=>'medicineType_form']) !!}
                                    @include('admin.type.form')
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="">
                        <button type="button" name="add" style="margin: 1px" id="add_data" class="btn btn-secondary">Add Type

                        </button>
                    </div>
                    <div class="card card-dark ">
                        <div class="card-header">
                            <h3 class="card-title">All Medicine Type</h3>
                        </div>

                    </div>
                    @include('admin.type.list')

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('script')

    <script type="text/javascript">
        {{--Datatable portion for showing data using datatable api--}}
        $(document).ready(function () {
            $('#medicineType_table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('medicineType.api') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "medicine_type"},

                    {"data": "action", orderable: false, searchable: false},
                    {"data": "checkbox", orderable: false, searchable: false}
                ]
            });

            /*end datatable show code*/

            //Modal portion for insert a new data

            $('#add_data').click(function () {
                $('#medicineTypeModal').modal('show');
                $('#medicineType_form')[0].reset();
                $('#form_output').html('');
                $('#button_action').val('insert');
                $('#action').val('Add');
            });

            /*end modal portion*/

            /*Code Portion for Submit and Update Data*/

            $('#medicineType_form').on('submit', function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    url: "{{ route('medicineType.postdata') }}",
                    method: "POST",
                    data: form_data,
                    dataType: "json",
                    success: function (data) {

                        $('#medicineTypeModal').modal('hide');
                        $('#medicineType_form')[0].reset();
                        $('#action').val('Add');
                        $('.modal-title').text('Add Data');
                        $('#button_action').val('insert');
                        $('#medicineType_table').DataTable().ajax.reload();

                    },
                })
            });

            /*End code portion for submit and update data*/

            /*Code portion for show editing data*/
            $(document).on('click', '.edit', function () {
                var id = $(this).attr("id");
                $('#form_output').html('');
                $.ajax({
                    url: "{{route('medicineType.fetchdata')}}",
                    method: 'get',
                    data: {id: id},
                    dataType: 'json',
                    success: function (data) {
                        $('#name').val(data.name);
                        // $('#description').val(data.description);
                        $('#medicineType_id').val(id);
                        $('#medicineTypeModal').modal('show');
                        $('#action').val('Edit');
                        $('.modal-title').text('Edit Data');
                        $('#button_action').val('update');
                    }
                });

            });

            /*End code portion for show editing Data*/

            /*Code Portion for delete single Data*/

            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('medicineType.removedata')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            alert(data);
                            $('#medicineType_table').DataTable().ajax.reload();
                        }
                    })
                } else {
                    return false;
                }
            });

            /*End code portion for Delete Single Data*/

            /*Code Portion for Mass Delete*/

            $(document).on('click', '#bulk_delete', function () {
                var id = [];
                if (confirm("Are you sure you want to Delete this data?")) {
                    $('.category_checkbox:checked').each(function () {
                        id.push($(this).val());
                    });
                    if (id.length > 0) {
                        $.ajax({
                            url: "{{ route('medicineType.massremove')}}",
                            method: "get",
                            data: {id: id},
                            success: function (data) {
                                alert(data);
                                $('#medicineType_table').DataTable().ajax.reload();
                            }
                        });
                    } else {
                        alert("Please select atleast one checkbox");
                    }
                }
            });
            /*End code Portion for Mass Delete*/
        });
    </script>
@stop
