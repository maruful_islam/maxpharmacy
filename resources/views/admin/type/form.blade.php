<div class="modal-header">
    <h4 class="modal-title">Add Medicine Type</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">

    <span id="form_output"></span>
    <div class="form-group">
        {!! Form::label('medicine_type','Medicine Type:', ['class' => 'control-label']) !!}
        {!! Form::text('medicine_type',null,['class' => 'form-control','placeholder'=>'Medicine Type']) !!}
    </div>

</div>
<div class="modal-footer">
    <input type="hidden" name="medicineType_id" id="medicineType_id" value="" />
    <input type="hidden" name="button_action" id="button_action"
           value="insert"/>
    <input type="submit" name="submit" id="action" value="Add"
           class="btn btn-primary"/>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close
    </button>
</div>
