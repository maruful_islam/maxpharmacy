@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Shelf List</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Shelf</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- left column -->
                <div class="col-md-5">
                    <div class="table-responsive">
                        <form method="post" id="dynamic_form">
                            <span id="result"></span>
                            <table class="table table-bordered table-striped" id="user_table">
                                <thead>
                                <tr>
                                    <th width="35%">Name</th>
                                    <th width="35%">Shelf Number</th>
                                    <th width="30%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>


                                </tfoot>

                            </table>
                            @csrf
                            <input type="submit" name="save" id="save" class="btn btn-primary" value="Save"/>
                        </form>
                    </div>

                </div>

                <div class="col-md-7">
                    <!-- Horizontal Form -->


                    <div class="card card-dark ">
                        <div class="card-header">
                            <h3 class="card-title">All Shelf</h3>
                        </div>
                    </div>
                    @include('admin.shelf.list')
                    @include('admin.shelf.form')


                </div>

            </div>
        </div>

    </section>

@stop
@section('script')

    <script type="text/javascript">

        $(document).ready(function () {

            $('#shelf_table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('shelf.api') }}",
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "shelf_number"},
                    {"data": "action", orderable: false, searchable: false},
                ]
            });

            /*end datatable show code*/
            var count = 1;

            dynamic_field(count);

            function dynamic_field(number) {
                html = '<tr>';
                html += '<td><input type="text" name="name[]" class="form-control" placeholder="Name" /></td>';
                html += '<td><input type="text" name="shelf_number[]" class="form-control" placeholder="Shelf No" /></td>';
                if (number > 1) {
                    html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                    $('#user_table').append(html);
                }
                else {
                    html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                    $('#user_table').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);
            });

            $(document).on('click', '.remove', function () {
                count--;
                $(this).closest("tr").remove();
            });

            $('#dynamic_form').on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: '{{ route("dynamic-field.insert") }}',
                    method: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function () {
                        $('#save').attr('disabled', 'disabled');
                    },
                    success: function (data) {
                        if (data.error) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<p>' + data.error[count] + '</p>';
                            }
                            $('#result').html('<div class="alert alert-danger">' + error_html + '</div>');
                        }
                        else {
                            dynamic_field(1);
                            $('#result').html('<div class="alert alert-success">' + data.success + '</div>');
                        }
                        $('#save').attr('disabled', false);
                        $('#shelf_table').DataTable().ajax.reload();
                    }
                })
            });
            /*Code Portion for delete single Data*/

            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "{{route('shelf.removedata')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
//                            alert(data);
                            $('#shelf_table').DataTable().ajax.reload();
                        }
                    })
                } else {
                    return false;
                }
            });

            /*End code portion for Delete Single Data*/

            /*Code portion for show editing data*/
            $(document).on('click', '.edit', function () {

                $('#shelfModal').modal('show');
                var id = $(this).attr("id");

                $.ajax({
                    url: "{{route('shelf.fetchdata')}}",
                    method: 'get',
                    data: {id: id},
                    dataType: 'json',
                    success: function (data) {
                        $('#name').val(data.name);
                        $('#shelf_number').val(data.shelf_number);
                        $('#shelf_id').val(id);
                        $('#shelfModal').modal('show');
                        $('.modal-title').text('Edit Data');
                        $('#action').val('Edit');
                        $('#button_action').val('update');
                    }
                });

            });

            /*End code portion for show editing Data*/
            //update data code
            $('#shelf_form').on('submit', function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    url: "{{ route('shelf.postdata') }}",
                    method: "POST",
                    data: form_data,
                    dataType: "json",
                    success: function (data) {


                        $('#shelfModal').modal('hide');
                        $('#shelf_form')[0].reset();
                        $('#action').val('Add');
                        $('.modal-title').text('Add Data');
                        $('#button_action').val('insert');
                        $('#shelf_table').DataTable().ajax.reload();


                    },
                    error: function () {

                    }
                })
            });

        });


    </script>
@stop
