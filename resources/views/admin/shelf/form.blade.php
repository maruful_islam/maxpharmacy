<div class="modal" id="shelfModal" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="shelf_form" method="post" data-toggle="validator"
                  enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <h3 class="modal-title"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="name" class="control-label">Name</label>

                        <input type="text" id="name" name="name" class="form-control" autofocus required>
                        <span class="help-block with-errors"></span>

                    </div>

                    <div class="form-group">
                        <label for="shelf_number" class="control-label">Shelf Number</label>

                        <input type="text" id="shelf_number" name="shelf_number" class="form-control"
                               autofocus
                               required>
                        <span class="help-block with-errors"></span>

                    </div>


                </div>


                <div class="modal-footer">
                    <input type="hidden" name="shelf_id" id="shelf_id" value=""/>
                    <input type="hidden" name="button_action" id="button_action"
                           value="insert"/>
                    <input type="submit" name="submit" id="action" value="Add"
                           class="btn btn-primary"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>
