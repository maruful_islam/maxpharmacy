@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')
@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchse Product List </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Medicine List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">

                            <h3 class="card-title">Update Purchase Details</h3>
                        </div>

                        {!! Form::model($purchase_details, ['method' => 'PUT','route' => ['purchase.updatedata', $purchase_details->id],'enctype'=>'multipart/form-data']) !!}
                        <div class="max-s1" style="padding: 1%">
                            <div class="row">
                                <span id="result"></span>
                                <div class="col-md-1.5">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Medicine Name', 'Medicine Name:', ['class' => 'control-label','value'=>"{{ old('medicine_name') }}" ]) !!}


                                        {!! Form::text('medicine_name',$purchase_details->medicine->medicine_name, ['class' => 'form-control','placeholder'=>'','readonly']) !!}

                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Batch', 'Batch', ['class' => 'control-label','value'=>"{{ old('purchase_batch_no') }}" ]) !!}
                                        {!! Form::text('purchase_batch_no', null, ['class' => 'form-control','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Expire Date', 'Expire Date', ['class' => 'control-label','value'=>"{{ old('expire_date') }}" ]) !!}
                                        {!! Form::date('expire_date', null, ['class' => 'form-control','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Medicine Type', 'Type', ['class' => 'control-label','value'=>"{{ old('medicine_type') }}" ]) !!}
                                        {!! Form::text('medicine_type', null, ['class' => 'form-control','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Pack Size', 'Pack Size', ['class' => 'control-label ','value'=>"{{ old('box_size') }}" ]) !!}
                                        {!! Form::text('box_size', null, ['class' => 'form-control box_size ','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Unit', 'Unit', ['class' => 'control-label ','value'=>"{{ old('purchase_unit') }}" ]) !!}
                                        {!! Form::text('purchase_unit', null, ['class' => 'form-control purchase_unit','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Unit Price', 'Unit Price', ['class' => 'control-label ','value'=>"{{ old('unit_price') }}" ]) !!}
                                        {!! Form::text('supplier_price', null, ['class' => 'form-control unit_price','placeholder'=>'','readonly']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                        {!! Form::label('Quantity', 'Quantity', ['class' => 'control-label','value'=>"{{ old('purchase_quantity') }}" ]) !!}
                                        {!! Form::text('purchase_quantity', null, ['class' => 'form-control purchase_quantity','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >


                                        {!! Form::label('Total', 'Total', ['class' => 'control-label ','value'=>"{{ old('') }}" ]) !!}
                                        {!! Form::text('purchase_total', null, ['class' => 'form-control purchase_total','placeholder'=>'']) !!}
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >



                                        {!! Form::hidden('purchase_id',null, ['class' => 'form-control','placeholder'=>'']) !!}

                                    </div>
                                </div>


                            </div>

                        </div>

                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('purchase.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                        {!! Form::close() !!}

                    </div>


                </div>


            </div>

        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">

        $(document).on('keyup', '.purchase_quantity', function () {

            var quantity = $('.purchase_quantity').val();
            var box_size = $('.box_size').val();
            var unit_price = $('.unit_price').val();
            var change_total = unit_price * box_size * quantity;
            $('.purchase_total').val(change_total);
            console.log('total', change_total);
            console.log('Grand total', change);


        });
    </script>
@stop
