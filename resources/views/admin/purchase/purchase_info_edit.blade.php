@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')
@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }
    </style>
@stop
@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{--<h1 style="text-align: center">Purchase Info Edit</h1>--}}
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Purchase Info Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 offset-md-2">

                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div class="card-header">

                            <h3 class="card-title">Edit Purchase Info</h3>
                        </div>
                        {!! Form::model($purchase, ['method' => 'PUT','route' => ['purchase_info.updatedata', $purchase->id],'enctype'=>'multipart/form-data']) !!}
                        <div class="row" style="padding: 1%">
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >

                                    {{ Form::label('Supplier:')}}

                                    {{Form::select("supplier_id",$repository->suppliers(),null,
                             [
                                "class" => "form-control select2 supplier_class",
                                "placeholder" => "Select Supplier",
                                "id"=>"supplier_id"
                             ])
                    }}


                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('purchase_date') ? 'has-error' : '' }} >
                                    {!! Form::label('Date', 'Date:', ['class' => 'control-label','value'=>"{{ old('name') }}" ]) !!}
                                    {!! Form::date('purchase_date', null, ['class' => 'form-control','placeholder'=>'Date']) !!}
                                    <span class="text-danger">{{ $errors->first('purchase_date') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
                                    {!! Form::label('Invoice No', 'Invoice No:', ['class' => 'control-label','value'=>"{{ old('name') }}" ]) !!}
                                    {!! Form::text('purchase_invoice_no', null, ['class' => 'form-control','placeholder'=>'Enter Number']) !!}
                                    <span class="text-danger">{{ $errors->first('purchase_invoice_no') }}</span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('Total Amount:') }}
                                    {{ Form::text('purchase_grand_total',$purchase->purchaseDetails->sum('purchase_total'),['class'=>'form-control purchase_grand_total'])}}
                                    <span class="text-danger">{{ $errors->first('purchase_grand_total') }}</span>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('Previous Paid Amount:') }}
                                    {{ Form::text('prev_paid',$purchase->purchase_paid_amount,['class'=>'form-control purchase_paid_amount','readonly'])}}
                                    <span class="text-danger">{{ $errors->first('prev_paid_amount') }}</span>

                                </div>
                                <div class="form-group ">
                                    {{ Form::label('payment:') }}
                                    {{ Form::text('purchase_paid_amount',null,['class'=>'form-control purchase_paid_amount'])}}
                                    <span class="text-danger">{{ $errors->first('purchase_paid_amount') }}</span>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    {{ Form::label('Due:') }}
                                    {{ Form::text('purchase_due_amount',null,['class'=>'form-control purchase_due_amount','readonly'])}}
                                    <span class="text-danger">{{ $errors->first('purchase_due_amount') }}</span>

                                </div>
                            </div>

                            <div class="card-footer">
                                {!! Form::submit('Update', ['class' => 'btn btn-primary','id'=>'btn_ctrl']) !!}
                                <a href="{{route('purchase.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">

        var grand_total = $('.purchase_grand_total').val();
        var paid = $('.purchase_paid_amount').val();
        var due = grand_total - paid;
        $('.purchase_due_amount').val(due);


    </script>
@stop
