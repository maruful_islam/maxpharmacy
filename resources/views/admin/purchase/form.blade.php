<div class="max-s1" style="padding: 1%">
    <div class="row">
        <span id="result"></span>
        <div class="col-md-4">
            <div class="form-group">

                {{ Form::label('Supplier:')}}

                {{Form::select("supplier_id",$repository->suppliers(),null,
         [
            "class" => "form-control select2 supplier_class",
            "placeholder" => "Select Supplier",
            "id"=>"supplier_id"
         ])
}}



            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('Date', 'Date:', ['class' => 'control-label','value'=>"{{ old('name') }}" ]) !!}
                {!! Form::text('purchase_date', date('d/m/Y'), ['class' => 'form-control','placeholder'=>'Date']) !!}

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('Invoice No', 'Invoice No:', ['class' => 'control-label','value'=>"{{ old('name') }}" ]) !!}
                {!! Form::text('purchase_invoice_no', null, ['class' => 'form-control','placeholder'=>'Enter Number']) !!}

            </div>
        </div>


    </div>
    <br>
    <br>
    <table class="table table-bordered table-striped">
        <thead>
        <tr style="text-align: center;">

            <th style="width:12%">Medicine</th>
            <th style="width:10%">Supp.Price</th>
            <th style="width:9%">Batch</th>
            <th style="width:14%">Exp.Date</th>
            <th style="width:9%">Type</th>
            <th style="width:9%">Pack Size</th>
            <th style="width:9%">Unit</th>
            <th style="width:9%">Unit Price</th>
            <th style="width:9%">Quantity</th>
            <th style="width:9%">Total Amount</th>
            <th style="width:4%">Action</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
        <tfoot>


        </tfoot>
    </table>
    <table class="table table-bordered table-striped test" id="user_table">

    </table>
    <div class="row">
        <div class="custom col-md-10" style="margin-bottom: -5%">
            <div class="form-group">

                {!! Form::textarea('purchase_notes', null, ['class' => 'form-control','placeholder'=>'Purchase Notes','style'=>'height:60px']) !!}

            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group" >
                {!! Form::text('purchase_grand_total', null, ['class' => 'form-control purchase_grand_total','id'=>'purchase_grand_total','placeholder'=>'Grand Total']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">


        </div>
        <div class="col-md-2">
            <div class="form-group" >
                {!! Form::text('purchase_paid_amount', null, ['class' => 'form-control purchase_paid_amount','placeholder'=>'Paid']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            <div class="form-group" >
                {!! Form::text('purchase_due_amount', null, ['class' => 'form-control purchase_due_amount','placeholder'=>'Due']) !!}

            </div>
        </div>


    </div>
    <div class="row">
        <div class="custom col-md-10">
        </div>
        <div class="col-md-2">
            {!! Form::submit('submit', ['class' => 'btn btn-primary btn-lg','id'=>'save']) !!}
        </div>


    </div>
</div>



