@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Medicine Purchase</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Purchase</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <span id="result"></span>
                        <div class="card-header">

                            <h3 class="card-title">Add Purchase</h3>
                        </div>


                        {!! Form::open(['id'=>'dynamic_form','method'=>'post'])  !!}
                        @include('admin.purchase.form')
                        {{ Form::close() }}

                    </div>


                </div>


            </div>
        </div>
    </section>

@stop
@section('script')
    <script type="text/javascript">


        //select2
        $(document).ready(function () {



            // code portion for dynamically field append
            var count = 1;

            dynamic_field(count);

            function dynamic_field(number) {
                html = '<tr>';
                html += '<td> <select class="form-control medicine_class select2" name="medicine_id[]" id="medicine_id" style="width: 160px;"><option value=""></option></select></td>';

                html += '<td><input type="text" name="supplier_price[]" class="form-control supplier_price" id="supplier_price" placeholder="Supplier Price" readonly/></td>';
                html += '<td><input type="text" name="purchase_batch_no[]" class="form-control" placeholder="Batch" /></td>';
                html += '<td><input type="date" name="expire_date[]" class="form-control" placeholder="Exp" /></td>';
                html += '<td><input type="text" name="medicine_type[]" class="form-control medicine_type" placeholder="Types" id="medicine_type" readonly/></td>';
                html += '<td><input type="text" name="box_size[]" class="form-control box_size" id="box_size" placeholder="Pack Size"  readonly/></td>';
                html += '<td><input type="text" name="purchase_unit[]" class="form-control unit" id="unit" placeholder="Unit"   readonly/></td>';
                html += '<td><input type="text" name="unit_price[]" class="form-control unit_price" id="unit_price" placeholder="Unit Price" /></td>';
                html += '<td><input type="text" name="purchase_quantity[]" class="form-control purchase_quantity" id="purchase_quantity" placeholder="Quantity" /></td>';
                html += '<td><input type="text" name="purchase_total[]" class="form-control purchase_total" id="purchase_total" placeholder="Total" /></td>';
                if (number > 1) {
                    html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                    $('#user_table').append(html);
                }
                else {
                    html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                    $('#user_table').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);

                appendMedicineName();


            });

            $(document).on('click', '.remove', function () {
                count--;
                $(this).closest("tr").remove();
            });
            //end code portion for dynamically append code


            $(".supplier_class").change(function () {
                appendMedicineName();
            });
            $(document).on('change', '.medicine_class', function () {
                id = $(this).val();
                var className = Math.floor(Math.random() * 1000).toString();
                console.log('check:', className);
                $(this).parents('tr').addClass(className);
                appendMedicineSingleData(id, className);


            });

            function appendMedicineName() {
                var id = $('#supplier_id').val();
                $.ajax({
                    url: "{{ route('select2_supplier_id') }}",
                    method: 'GET',
                    data: {id: id},
                    success: function (data) {

                        $.each(data, function (key, value) {

                            $('.medicine_class').append('<option value="' + key + '">' + value + '<option>');


                        })


                    }
                })
            }

            function appendMedicineSingleData(id, className) {
                $.ajax({
                    url: "{{ route('purchase.select2_medicine_id') }}",
                    method: 'GET',
                    data: {id: id},
                    success: function (data) {
                        console.log(id);
                        console.log(data[0]['supplier_price']);
                        //return data[0]['supplier_price'];
                        $('.' + className).find('.supplier_price').val(data[0]['supplier_price']);
                        $('.' + className).find('.medicine_type').val(data[0]['medicine_type']);
                        $('.' + className).find('.unit').val(data[0]['unit']);
                        $('.' + className).find('.box_size').val(data[0]['box_size']);
                        $('.' + className).find('.unit_price').val(data[0]['supplier_price']);


                        $(document).on('keyup', '.purchase_quantity', function () {
                            var cl = $(this).parents('tr').attr('class');
                            var pack_size = $('.' + cl).find('.box_size').val();
                            var unit_price = $('.' + cl).find('.unit_price').val();
                            var quantity = $('.' + cl).find('.purchase_quantity').val();
                            var total_amount = parseFloat(pack_size) * parseFloat(quantity) * parseFloat(unit_price);
                            $('.' + cl).find('.purchase_total').val(total_amount.toFixed(2));


                            var grand_total = 0;
                            $('.purchase_total').each(function () {
                                grand_total += Number($(this).val());
                            });

                            $('.purchase_grand_total').val(grand_total.toFixed(2));


                        });

                        $(document).on('keyup', '.purchase_paid_amount', function () {


                            grand_total = $('.purchase_grand_total').val();
                            var paid_amount = $('.purchase_paid_amount').val();
                            var due_total = parseFloat(grand_total) - parseFloat(paid_amount);
                            $('.purchase_due_amount').val(due_total.toFixed(2));
                        });

                    }
                })
            }


            $('#dynamic_form').on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: '{{ route("purchase.storedata") }}',
                    method: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.error) {
                            var error_html = '';
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<p>' + data.error[count] + '</p>';
                            }
                            $('#result').html('<div class="alert alert-danger">' + error_html + '</div>');
                        }
                        else {
                            dynamic_field(1);
                            $('#result').html('<div class="alert alert-success">' + data.success + '</div>');
                        }

                    }
                })
            });

            $('.select2').select2();

        });


    </script>
@stop
