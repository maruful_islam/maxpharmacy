@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Purchase</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All Purchase List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-dark">
                        <div class="card-header">

                            <h3 class="card-title">All Purchase List</h3>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Purchase Date</th>
                                <th>Invoice No</th>
                                <th>Purchase ID</th>
                                <th>Supplier</th>
                                <th>Total</th>
                                <th>Due</th>
                                <th>Notes</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($all_data as $single_data)
                                @php
                                    $i++;
                                @endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$single_data->purchase_date}}</td>
                                    <td>{{$single_data->purchase_invoice_no}}</td>
                                    <td>{{$single_data->id}}</td>
                                    <td>{{$single_data->supplier->name}}</td>
                                    <td>
                                        @if($single_data->purchaseDetails)
                                            {{$single_data->purchaseDetails->sum('purchase_total')}}


                                        @endif

                                    </td>
                                    <td>

                                        @php
                                            if(isset($single_data)){
                                                     $total=$single_data->purchaseDetails->sum('purchase_total');
                                                     $paid=$single_data->sum('purchase_paid_amount');
                                                     $due=$total-$paid;
                                                 }

                                        @endphp

                                        {{$due}}
                                    </td>
                                    <td>{{$single_data->purchase_notes}}</td>


                                    <td>


                                        <a class="btn btn-primary"
                                           href="{{route('purchase.showdata',$single_data->id)}}">Show</a>
                                        <a class="btn btn-info"
                                           href="{{route('purchase.purchase_info.editdata',$single_data->id)}}">Edit</a>


                                        {!! Form::open(['method' => 'DELETE','route' => ['purchase.deletedata', $single_data->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger','onClick'=>'return confirm("are you sure to delete?")']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$all_data->render()}}

                    </div>



                </div>


            </div>
        </div>
    </section>

@stop


@section('script')
    <script type="text/javascript">

    </script>
@stop
