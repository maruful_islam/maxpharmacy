@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchase Details</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Purchase Details</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">


            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-dark">
                    <div class="card-header">

                        <h3 class="card-title">Purchase Details</h3>
                    </div>
                    <table id="supplier" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Medicine Name</th>
                            <th>Supplier Price</th>
                            <th>Batch No</th>
                            <th>Expire Date</th>
                            <th>Type</th>
                            <th>Pack Size</th>
                            <th>Unit</th>
                            <th>Unit Price</th>
                            <th>Quantity in Box</th>
                            <th>Quantity in Piece</th>
                            <th>Total Amount</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php $i=0; @endphp
                        @foreach($all_data as $single_data)
                            @php $i++; @endphp
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$single_data->medicine->medicine_name}}</td>
                                <td>{{$single_data->supplier_price}}</td>
                                <td>{{$single_data->purchase_batch_no}}</td>
                                <td>{{$single_data->expire_date}}</td>
                                <td>{{$single_data->medicine_type}}</td>
                                <td>{{$single_data->box_size}}</td>
                                <td>{{$single_data->purchase_unit}}</td>
                                <td>{{$single_data->medicine->supplier_price}}</td>
                                <td>{{$single_data->purchase_quantity}}</td>
                                <td>@php
                                        if(isset($single_data)){
                                            $box_size=$single_data->box_size;
                                            $quantity=$single_data->purchase_quantity;
                                            $total_piece=$box_size*$quantity;
                                            }
                                    @endphp
                                    {{$total_piece}}

                                </td>
                                <td>{{$single_data->purchase_total}}</td>

                                <td>
                                    <a class="btn btn-primary" href="{{route('purchase.editdata',$single_data->id)}}">Edit</a>

                                    {!! Form::open(['method' => 'DELETE','route' => ['purchase.product.deletedata', $single_data->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger','onClick'=>'return confirm("are you sure to delete?")']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>


                </div>
                <a href="{{route('purchase.index')}}" class="btn btn-danger btn-xs">Cancel</a>


            </div>


        </div>
    </section>

@stop


@section('script')
    <script type="text/javascript">

    </script>
@stop
