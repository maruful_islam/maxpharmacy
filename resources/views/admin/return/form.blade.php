@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('style')
    <style type="text/css">
        #image_preview {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }

        .initiallyHidden {
            display: none;
        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Point of Sale</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Pos</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->

                    {{ Form::open(['action'=>['ReturnProductController@storeReturnProduct'],'method'=>'post']) }}

                    <div class="card card-dark" style="padding-bottom: 10px">
                        <span id="result"></span>
                        <div class="card-header">
                            <h3 class="card-title">Return Sale</h3>
                        </div>

                        <div class="card-body">

                            <div class="row">
                                <span id="result"></span>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('return', 'Return Date', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('return_date',date('Y-m-d'), ['class' => 'form-control text-center','id' => 'return_date']) !!}

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">

                                        {!! Form::label('Customer Name', 'Customer Name:', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('', $sale->customers->customer_name, ['class' => 'form-control']) !!}
                                        {!! Form::hidden('sales_id',$sale->id, ['class' => 'form-control']) !!}

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('date', 'Sales Date:', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('sales_date', $sale->sale_invoice_date, ['class' => 'form-control text-center']) !!}

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('GrandTotal', 'Grand Total:', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('grand_total', $sale->saleDetails->sum('sale_total_amount'), ['class' => 'form-control text-center grand_total','id'=>'grand_total']) !!}

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('PaidAmount', 'Paid Amount:', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('paid_amount', $sale->sale_paid_amount, ['class' => 'form-control text-center']) !!}

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('return', 'Return Amount', ['class' => 'control-label' ]) !!}
                                        {!! Form::text('return_amount', null, ['class' => 'form-control text-center return_amount','id' => 'return_amount']) !!}

                                    </div>
                                </div>

                            </div>
                        </div>

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr style="text-align: center;">
                                <th style="width:8%">Medicine</th>
                                <th style="width:8%">BatchID</th>
                                <th style="width:8%">ExpireDate</th>
                                <th style="width:9%">Unit Price</th>
                                <th style="width:9%">Sold Quantity</th>
                                <th style="width:9%">Total</th>
                                <th style="width:9%">Return Quantity</th>
                                <th style="width:9%">Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($saleDetails as $saleDetail)
                                <tr>
                                    {!! Form::hidden('medicine_id[]',$saleDetail->medicine->id, ['class' => 'form-control']) !!}
                                    <td> {{Form::text("",$saleDetail->medicine->medicine_name, ["class" => "form-control text-center select2 sale_medicine_class", "placeholder" => "Select Medicine",   "id"=>"medicine_id" ,'readonly'])}}  </td>
                                    <td> {{Form::text("purchase_batch_no[]",$saleDetail->purchase_batch_no, ["class" => "form-control text-center select2 purchase_batch_no",  "id"=>"purchase_batch_no" ,'readonly'])}}  </td>
                                    @php

                                        $expire_date=\App\PurchaseDetail::query()->where('purchase_batch_no',$saleDetail->purchase_batch_no)->Where('medicine_id',$saleDetail->medicine_id)->first();


                                    @endphp

                                    <td> {{Form::text("expire_date[]",$expire_date->expire_date, ["class" => "form-control text-center select2 expire_date",  "id"=>"expire_date" ,'readonly'])}}  </td>
                                    <td> {{Form::text("unit_price[]",$saleDetail->medicine->selling_price, ["class" => "form-control text-center  unit_price",'medicineId'=>$saleDetail->id, "id"=>"unit_price".$saleDetail->id,'readonly'])}}  </td>
                                    <td> {{Form::text("sold_quantity[]",$saleDetail->sale_quantity, ["class" => "form-control text-center  sold_quantity", "id"=>"sold_quantity",'readonly'])}}  </td>
                                    <td> {{Form::text("sale_total_amount[]",$saleDetail->sale_total_amount, ["class" => "form-control  text-center sale_total_amount", "id"=>"sale_total_amount",'readonly'])}}  </td>
                                    <td> {{Form::text("return_quantity[]",null, ['id'=>'return_quantity',"class" => "form-control text-center  return_quantity",'medicineId'=>$saleDetail->id, "id"=>"return_quantity".$saleDetail->id])}}  </td>
                                    <td> {{Form::text("total_amount[]",0, ["class" => "form-control text-center total_amount",'medicineId'=>$saleDetail->id, "id"=>"total_amount".$saleDetail->id])}}  </td>
                                    {{--                                    {{Form::hidden("total_amount[]",0, ["class" => "form-control text-center total_amount",'medicineId'=>$saleDetail->id, "id"=>"total_amount".$saleDetail->id])}}--}}
                                    {{Form::hidden("box_size[]",$saleDetail->medicine->box_size, ["class" => "form-control text-center box_size",'medicineId'=>$saleDetail->id, "id"=>"box_size".$saleDetail->id])}}
                                </tr>

                            @endforeach
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <div class="row">
                            <div class="col-md-2 offset-5">
                                {!! Form::submit('submit', ['class' => 'btn btn-primary btn-sm','id'=>'save']) !!}
                            </div>


                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>


        {{--</section>--}}

        @stop
        @section('script')
            <script type="text/javascript">


                $(".return_quantity").keyup(function (event) {

                    event.preventDefault();

                    var medicineId = $(this).attr("medicineId");

                    var return_qty = $("#return_quantity" + medicineId).val();
                    var unit_price = $("#unit_price" + medicineId).val();
                    var box_size = $("#box_size" + medicineId).val();
                    console.log('check', box_size);
                    var editedQty = 0;
                    var total_return_amount = 0;


                    var total_amount = return_qty * unit_price * box_size;
                    $("#total_amount" + medicineId).val(total_amount);

                    $(".return_qty").each(function () {
                        editedQty += parseInt($(this).val());
                    });

                    $(".total_amount").each(function () {
                        total_return_amount += parseInt($(this).val());
                    });

                    $(".return_amount").val(total_return_amount);


                });




            </script>
@stop
