@extends('admin.layouts.fixed')

@section('title','Product Wise Stock Report | Max Pharmacy')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card bg-dark text-white">
                        <div class="card-body" style="text-align: center;font-size: 20px">Medicine Stock Report
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="card card-secondary" style="padding-top: 2%">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                    <tr style="text-align: center;">
                        <th style="width:8%">Sr.</th>
                        <th style="width:8%">Medicine Name</th>
                        <th style="width:8%">Customer Name</th>
                        <th style="width:8%">Invoice Id</th>
                        <th style="width:8%">BatchID</th>
                        <th style="width:8%">ExpireDate</th>
                        <th style="width:8%">Return Date</th>
                        <th style="width:9%">Total Quantity</th>

                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i=1;
                    @endphp

                    @foreach($all_data as $single_data)
                        <tr style="text-align: center">

                            <td>{{$i}}</td>
                            <td>{{($single_data->medicine->medicine_name)}}</td>
                            <td>{{$single_data->sale->customers->customer_name}}</td>
                            <td>{{$single_data->sale->sale_invoice_no}}</td>
                            <td>{{$single_data->purchase_batch_no}}</td>
                            <td>{{$single_data->expire_date}}</td>
                            <td>{{$single_data->return_date}}</td>
                            <td>{{$single_data->return_quantity}}</td>


                        </tr>
                        @php $i++; @endphp
                    @endforeach
                    </tbody>
                    <tfoot>


                    </tfoot>
                </table>


            </div>
        </div>
    </section>
@endsection

@section('script')
 <script type="text/javascript">

        $(document).ready(function() {
            $('#myTable').DataTable( {
       } );

        } );
    </script>
@stop
