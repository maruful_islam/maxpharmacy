@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Customer List</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Customer List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-5">



                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">Add Customer</h3>
                        </div>

                        {!! Form::open(array('route' => 'customer.storedata','method'=>'POST','enctype'=>'multipart/form-data' ) ) !!}
                        @include('admin.customer.form')
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                        {{ Form::close() }}
                    </div>


                </div>

                <div class="col-md-7">
                    <!-- Horizontal Form -->

                    <div class="card card-dark">
                        <div class="card-header">
                            <h3 class="card-title">All Customer</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search Customer Data" />
                    </div>
                    @include('admin.customer.list')


                </div>

            </div>
        </div>
    </section>

@stop
@section('script')

    <script type="text/javascript">

        $(document).ready(function () {

            fetch_customer_data();

            function fetch_customer_data(query = '') {
                $.ajax({
                    url: "{{ route('search.action') }}",
                    method: 'GET',
                    data: {query: query},
                    success: function (data) {
                        console.log(data);
                        $('tbody').html(data);
//                        $('#total_records').text(data.total_data);
                    }
                })
            }

            $(document).on('keyup', '#search', function () {
                var query = $(this).val();
                fetch_customer_data(query);
            });
        });
    </script>

@stop
