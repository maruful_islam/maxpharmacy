<div class="card-body">

<div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }} >
    {!! Form::label('Name', 'Name:', ['class' => 'control-label','value'=>"{{ old('name') }}" ]) !!}
    {!! Form::text('customer_name', null, ['class' => 'form-control','placeholder'=>'Name']) !!}
    <span class="text-danger">{{ $errors->first('name') }}</span>
</div>
<div class="form-group" {{ $errors->has('email') ? 'has-error' : '' }}>
    {!! Form::label('email', 'Email:', ['class' => 'control-label','value'=>"{{ old('email') }}"]) !!}
    {!! Form::email('email', null, ['class' => 'form-control','Placeholder'=>'Enter Your Email']) !!}
    <span class="text-danger">{{ $errors->first('email') }}</span>
</div>
<div class="form-group" {{ $errors->has('details') ? 'has-error' : '' }}>
    {!! Form::label('Details', 'Details:', ['class' => 'control-label','value'=>"{{ old('descrption') }}" ]) !!}
    {!! Form::textarea('details', null, ['class' => 'form-control','placeholder'=>'Address','style'=>'height:150px']) !!}
    <span class="text-danger">{{ $errors->first('details') }}</span>
</div>

<div class="form-group" {{ $errors->has('phone') ? 'has-error' : '' }}>
    {!! Form::label('Phone', 'Phone Number:', ['class' => 'control-label','value'=>"{{ old('phone') }}"]) !!}
    {!! Form::text('customer_phone_number', null, ['class' => 'form-control','Placeholder'=>'Enter Your Phone Number']) !!}
    <span class="text-danger">{{ $errors->first('phone') }}</span>
</div>



</div>