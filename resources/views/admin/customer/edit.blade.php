@extends('admin.layouts.fixed')

@section('title','AdminLTE 3 | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Customer List</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Customer List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-5">

                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">

                            <h3 class="card-title">Update Customer</h3>
                        </div>

                        {!! Form::model($customer, ['method' => 'PUT','route' => ['customer.editdata', $customer->id],'enctype'=>'multipart/form-data']) !!}
                        @include('admin.customer.form')
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('customer.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                        {!! Form::close() !!}

                    </div>


                </div>

                <div class="col-md-7">
                    <!-- Horizontal Form -->

                    <div class="card card-info ">
                        <div class="card-header">
                            <h3 class="card-title">All Customer</h3>
                        </div>
                    </div>
                    @include('admin.customer.list')


                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('script')

    <script type="text/javascript">

        $(document).ready(function () {
            save_method = "add";
            $('#user_uploaded_image').hide();
            var table = $('#supplier').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('supplier.api') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'customer_name', name: 'customer_name'},
                    {data: 'email', name: 'email'},
                    {data: 'customer_phone_number', name: 'customer_phone_number'},
                    {data: 'logo', name: 'logo'},
                    {
                        data: 'action', name: 'action', orderable: false, searchable: false, "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    }
                ]
            });


//store data
            $('#supplier_form').on('submit', function (event) {
                event.preventDefault();
                // alert($(this).serialize());
                var id = $('#id').val();
                if (save_method == 'add') url = '{{route('store.supplier')}}';
                else url = "{{ url('supplier') . '/' }}" + id;

                $.ajax({
                    type: "POST",
                    url: url,
                    // data:$(this).serialize(),
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        table.ajax.reload();
                        $('#supplier_form')[0].reset();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                    },
                    error: function () {
                        swal({
                            title: 'Oops...',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        })
                    }
                });
            })


        })

        function editData(id) {
            save_method = 'edit';
            $('#user_uploaded_image').show();
            $('#btn_ctrl').val("Update");
            $('input[name=_method]').val('PATCH');
            $('#supplier_form')[0].reset();
            $.ajax({
                url: "{{ url('supplier') }}" + '/' + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('.card-title').text('Edit Contact');
                    $('#id').val(data.id);
                    $('#name').val(data.name);
                    $('#email').val(data.email);
                    $('#phone').val(data.phone);
                    $('#details').val(data.details);
                    console.log(data.photo);
                    var ename = "{{ asset("upload/photo") }}" + "/" + data.logo;

                    $('#user_uploaded_image').attr("src", ename);


                },
                error: function () {
                    alert("Nothing Data");
                }
            });
        }

        function deleteData(id) {
            // alert("hello");

            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $.ajax({
                    url: "{{ url('supplier') }}" + '/' + id,
                    type: "POST",
                    data: {'_method': 'DELETE', '_token': csrf_token},
                    success: function (data) {

                        table.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                    },
                    error: function () {
                        swal({
                            title: 'Oops...',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        })
                    }
                });
            });

        }


    </script>
@stop
