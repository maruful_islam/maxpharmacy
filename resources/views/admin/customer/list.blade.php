<table id="customer" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Details</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($customer_data as $customer)
        <tr>
            <td>{{ $customer->id }}</td>
            <td>{{ $customer->customer_name}}</td>
            <td>{{ $customer->email}}</td>
            <td>{{ $customer->customer_phone_number}}</td>
            <td>{{ $customer->details}}</td>
            <td>


                <a class="btn btn-primary" href="{{ route('customer.editdata',$customer->id) }}">Edit</a>

                {!! Form::open(['method' => 'DELETE','route' => ['customer.deletedata', $customer->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach

    </tbody>

</table>
{{$customer_data->render()}}


