@extends('admin.layouts.fixed')

@section('title','Product Wise Stock Report | Max Pharmacy')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card bg-dark text-white">
                        <div class="card-body" style="text-align: center;font-size: 20px">Medicine Stock Report
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="card card-secondary" style="padding-top: 2%">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                    <tr style="text-align: center;">
                        <th style="width:8%">Sr.</th>
                        <th style="width:8%">Supplier</th>
                        <th style="width:8%">Medicine</th>
                        <th style="width:8%">Medicine Type</th>
                        <th style="width:9%">In Quantity</th>
                        <th style="width:9%">Sold Quantity</th>
                        <th style="width:9%">Stock</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i=1;
                    @endphp
                    @foreach($all_data as $single_data)
                        <tr style="text-align: center">

                            <td>{{$i}}</td>
                            <td>{{$single_data->supplier->name}}</td>
                            <td>{{$single_data->medicine_name}}</td>
                            <td>{{$single_data->medicine_type}}</td>
                            <td>{{$single_data->purchase_details()->sum('purchase_quantity')}}</td>
                            <td>{{$single_data->sale_details()->sum('sale_quantity') }}</td>
                            @php
                            $stock=($single_data->purchase_details()->sum('purchase_quantity'))-($single_data->sale_details()->sum('sale_quantity'));
                            @endphp
                            <td>{{$stock}}</td>


                        </tr>
                        @php $i++; @endphp
                    @endforeach
                    </tbody>
                    <tfoot>


                    </tfoot>
                </table>


            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function() {
            $('#myTable').DataTable( {
                "paging":   false,
                dom: 'Bfrtip',
                buttons: [
                    'print',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    }
                ]
            } );

        } );
    </script>
@stop
