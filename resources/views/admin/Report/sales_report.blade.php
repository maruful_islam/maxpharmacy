@extends('admin.layouts.fixed')

@section('title','Sales Report | Max Pharmacy')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card bg-dark text-white">
                        <div class="card-body" style="text-align: center;font-size: 20px">Sales Report
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['method'=>'post','files'=>true,'url'=>route('sales.report')])  !!}
            <div class="row">
                <div class="offset-3 col-md-2">
                    {{ Form::label('From:') }}<span style="color: red;font-size: larger">*</span>
                    {{ Form::text('date_from',date('d/m/Y'),['class'=>'form-control'])}}
                </div>
                <div class="col-md-2">
                    {{ Form::label('To:') }}<span style="color: red;font-size: larger">*</span>
                    {{ Form::text('date_to',date('d/m/Y'),['class'=>'form-control'])}}
                </div>
                <div class="col-md-1" style="padding-top: 2%">
                    {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                    {{ Form::close() }}
                </div>
            </div>
            <br>
            <br>
            <div class="card card-secondary" style="padding-top: 2%">
                <table id="myTable" class="table table-bordered table-hover display">
                    <thead>
                    <tr style="text-align: center;">
                        <th style="width:8%">Sr.</th>
                        <th style="width:8%">Sales Date</th>
                        <th style="width:8%">Invoice No</th>
                        <th style="width:9%">Customer Name</th>
                        <th style="width:9%" class="sum">Total Amount</th>

                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i=1;
                        $total = 0;
                    @endphp
                    @foreach($all_data as $single_data)
                        @php
                            $total += $single_data->saleDetails->sum('sale_total_amount');
                        @endphp
                        <tr style="text-align: center">
                            <td>{{$i}}</td>
                            <td>{{$single_data->sale_invoice_date}}</td>
                            <td>{{$single_data->sale_invoice_no}}</td>
                            <td>{{$single_data->customers->customer_name}}</td>
                            <td>{{$single_data->saleDetails()->sum('sale_total_amount') }}</td>

                        </tr>

                        </tr>
                        @php $i++; @endphp
                    @endforeach
                    </tbody>

                    <tfoot>
                    <th colspan="4" style="text-align: center">Total</th>
                    <th colspan="1" style="text-align: center">{{$total}}</th>
                    </tfoot>
                </table>

            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {
            $('#myTable').DataTable({
                "paging": false,
                dom: 'Bfrtip',
                buttons: [
                    'print',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    }
                ]

            });


        });
    </script>

@stop
