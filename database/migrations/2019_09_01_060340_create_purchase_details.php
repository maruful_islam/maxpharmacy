<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('purchase_id');
            $table->unsignedInteger('medicine_id');
            $table->float('box_size', '25');
            $table->integer('supplier_price');
            $table->integer('purchase_batch_no');
            $table->string('expire_date', '25');
            $table->string('medicine_type', '25');
            $table->string('purchase_unit');
            $table->integer('unit_price');
            $table->integer('purchase_quantity');
            $table->integer('purchase_total');
            $table->foreign('purchase_id')->references('id')->on('purchase')->onDelete('cascade');
            $table->foreign('medicine_id')->references('id')->on('medicines')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
    }
}
