<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplier_id')->unsigned();
            $table->unsignedInteger('category_id')->unsigned();
            $table->unsignedInteger('shelf_id')->unsigned();
            $table->string('medicine_name', '30');
            $table->string('medicine_type', '30');
            $table->string('medicine_details', '30');
            $table->string('generic_name', '30');
            $table->string('unit');
            $table->integer('box_size');
            $table->float('supplier_price', '30');
            $table->float('selling_price', '30');
            $table->string('medicine_image', '30');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
