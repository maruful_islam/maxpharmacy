<?php

namespace App\Repositories;


use App\Customer;
use App\Medicine;
use App\MedicineType;
use App\MedicineUnit;
use App\Supplier;

/**
 * Class InventoryRepository.
 */
class PharmacyRepository
{

    public function suppliers()
    {
        return Supplier::all()->pluck('name', 'id');
    }

    public function medicines()
    {
        return Medicine::query()->pluck('medicine_name', 'id');
    }

    public function customers()
    {
        return Customer::query()->pluck('customer_phone_number', 'id');
    }

    public function types()
    {
        return MedicineType::all()->pluck('medicine_type', 'id');
    }

    public function units()
    {
        return MedicineUnit::all()->pluck('medicine_unit', 'id');
    }

}
