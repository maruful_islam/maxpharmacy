<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    protected $fillable = ['customer_id', 'sale_invoice_no', 'sale_invoice_date','sale_paid_amount', 'sale_invoice_discount', 'sale_notes'];

    public function customers()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function saleDetails()
    {

        return $this->hasMany(SaleDetail::class);
    }

    public function returnProducts()
    {

        return $this->hasMany(ReturnProduct::class);
    }

}
