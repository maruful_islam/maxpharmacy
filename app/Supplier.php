<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'details', 'logo'];

    public function medicines()
    {
        return $this->hasMany(Medicine::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}
