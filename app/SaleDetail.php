<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $fillable = ['sale_id', 'medicine_id', 'purchase_batch_no', 'unit_price', 'sale_quantity', 'sale_total_amount'];

    public function sale()
    {
        return $this->belongsTo(Sale::class);

    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class);

    }

}
