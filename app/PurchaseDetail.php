<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $fillable = ['purchase_id', 'medicine_id', 'supplier_price', 'purchase_batch_no', 'expire_date', 'medicine_type', 'purchase_unit', 'purchase_quantity', 'purchase_total', 'box_size', 'unit_price'];


    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    public function medicine()
    {
        return $this->belongsTo(Medicine::class,'medicine_id');
    }

}
