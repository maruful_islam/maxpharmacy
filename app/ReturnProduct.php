<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnProduct extends Model
{
    protected $fillable = ['sales_id', 'medicine_id','return_date','total_amount','purchase_batch_no','expire_date', 'return_quantity', 'unit_price'];

    public function sale()
    {
        return $this->belongsTo(Sale::class,'sales_id');
    }
    public function medicine(){

        return $this->belongsTo(Medicine::class,'medicine_id');
    }

}
