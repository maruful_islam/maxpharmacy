<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['pharmacy_name', 'pharmacy_email', 'pharmacy_phone', 'pharmacy_address', 'pharmacy_website', 'pharmacy_logo'];
}
