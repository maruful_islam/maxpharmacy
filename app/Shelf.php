<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shelf extends Model
{
    protected $fillable = ['name', 'shelf_number'];
//    protected $table = 'shelfs';

    public function medicines()
    {
        return $this->hasMany(Medicine::class);
    }

}
