<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicineUnit extends Model
{
    protected $fillable = ['medicine_unit'];
}
