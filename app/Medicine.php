<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model

{
    protected $fillable = ['medicine_name', 'medicine_type', 'medicine_details', 'selling_price', 'generic_name',
        'unit', 'medicine_image', 'batch_number', 'box_size', 'supplier_price', 'supplier_id',
        'category_id', 'shelf_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);

    }

    public function shelf()
    {
        return $this->belongsTo(Shelf::class);
    }

    public function purchase_details()
    {
        return $this->hasMany(PurchaseDetail::class, 'medicine_id');

    }

    public function sale_details()
    {
        return $this->hasMany(SaleDetail::class);

    }

    public function return_products()
    {
        return $this->hasMany(ReturnProduct::class);

    }
}
