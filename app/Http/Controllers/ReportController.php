<?php

namespace App\Http\Controllers;

use App\Medicine;
use App\Purchase;
use App\PurchaseDetail;
use App\Repositories\PharmacyRepository;
use App\Sale;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $repository;

    public function __construct(PharmacyRepository $repository)
    {
        $this->repository = $repository;

    }

    public function index()

    {
        $all_data = Medicine::all();

        $repository = $this->repository;


        return view('admin.report.product_stock', compact('repository', 'all_data'));
    }

    public function stockOutReport()

    {
        $all_data = Medicine::all();

        $repository = $this->repository;


        return view('admin.report.stock_out', compact('repository', 'all_data'));
    }

    public function expiredProduct()
    {
        $todayDate = date("Y-m-d");
        $all_data = PurchaseDetail::query()->select('medicine_id','purchase_batch_no','expire_date')->where('expire_date','<',$todayDate)->get();
//dd($all_data);


        return view('admin.report.product_expired', compact('repository', 'all_data'));


    }


    public function salesReport(Request $request)
    {
        $date1 = strtr($request->date_from, '/', '-');
        $date2 = strtr($request->date_to, '/', '-');
        $all_data = Sale::whereBetween('sale_invoice_date', [$date1, $date2])->get();
        return view('admin.report.sales_report', compact('all_data'));

    }

    public function purchaseReport(Request $request)

    {
        $date1 = strtr($request->date_from, '/', '-');
        $date2 = strtr($request->date_to, '/', '-');
        $all_data = Purchase::whereBetween('purchase_date', [$date1, $date2])->get();
        return view('admin.report.purchase_report', compact('all_data'));

    }

    public function stockBatchWise()

    {
        $all_data = PurchaseDetail::all();

        return view('admin.report.product_stock_batch_wise', compact( 'all_data'));
    }

}
