<?php

namespace App\Http\Controllers;

use App\Customer;
use App\PurchaseDetail;
use App\Repositories\PharmacyRepository;
use App\Sale;
use App\SaleDetail;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Medicine;
use Validator;
use Yajra\DataTables\DataTables;

class PosController extends Controller
{
    private $repository;

    public function __construct(PharmacyRepository $repository)
    {
        $this->repository = $repository;

    }

    public function index()
    {
        $allData = Sale::all();
        return view('admin.pos.index', compact('allData'));
    }

    public function createData()

    {
        $repository = $this->repository;
        return view('admin.pos.create', compact('repository'));
    }

    public function getBatchId(Request $request)
    {
        $id = $request->id;

        $data = PurchaseDetail::query()->where('medicine_id', $id)->pluck('purchase_batch_no');
//        dd($data);

        return $data;
//        return response()->json($data);

    }

    public function getBatchData(Request $request)
    {
        $medicine_id = $request->medicine_id;
        $batch_id = $request->id;
        $data = Medicine::query()->select('medicine_type', 'unit', 'box_size', 'selling_price')->where('id', $medicine_id)->get();

        $total_purchase = PurchaseDetail::query()->where('medicine_id', $medicine_id)->where('purchase_batch_no', $batch_id)->sum('purchase_quantity');
        $expire_date = PurchaseDetail::query()->where('medicine_id', $medicine_id)->where('purchase_batch_no', $batch_id)->get();

        $total_sales = SaleDetail::query()->where('medicine_id', $medicine_id)->where('purchase_batch_no', $batch_id)->sum('sale_quantity');
        $stock_avail = intVal($total_purchase) - intVal($total_sales);


        $expire_date = $expire_date[0]['expire_date'];
//        dd($stock_avail);

        $optimum = array(
            'expire_date' => $expire_date,
            'stock_avail' => $stock_avail,
        );

        $temp1 = json_decode($data);
        $data = array_merge($temp1, $optimum);
        $data['expire_date'] = $expire_date;
        $data['stock_avail'] = $stock_avail;


        return response()->json($data);

    }


    public function storeData(Request $request)
    {
        if ($request->ajax()) {
            $rules = array(

                'sale_invoice_date' => 'required',
                'sale_paid_amount' => 'required',
                'medicine_id.*' => 'required',
                'sale_quantity.*' => 'required',
            );
            $error = Validator::make($request->all(), $rules);

            if ($error->fails()) {
                return response()->json([
                    'error' => $error->errors()->all()
                ]);
            }
            if ($request->customer_id == null AND $request->customer_name == null) {
                return response()->json([
                    'error' => ['Customer Data Should be Given']
                ]);
            } elseif ($request->customer_id == null) {

                $data_new_customer = new Customer();
                $data_new_customer['customer_name'] = $request->customer_name;
                $data_new_customer['customer_phone_number'] = $request->customer_phone_number;
                $data_new_customer->save();
                $customer_id = $data_new_customer->id;
            } else {
                $customer_id = $request->customer_id;
            }

            $data = new Sale;
            $data['customer_id'] = $customer_id;
            $data['sale_invoice_date'] = $request->sale_invoice_date;

            $data['sale_invoice_no'] = Date('Ymd') . mt_rand(100000, 900000);
            $data['sale_invoice_discount'] = $request->sale_invoice_discount;
            $data['sale_paid_amount'] = $request->sale_paid_amount;
            $data['sale_notes'] = $request->sale_notes;
            $data->save();
            $sale_id = $data->id;


            //dd($sale_id);
            //loop data
            $medicine_id = $request->medicine_id;
            $sale_quantity = $request->sale_quantity;
            $purchase_batch_no = $request->batch_id;
            $sale_total_amount = $request->sale_total_amount;
            $unit_price = $request->unit_price;


            for ($count = 0; $count < count($medicine_id); $count++) {

                $data_sale_details = array(
                    'sale_id' => $sale_id,
                    'medicine_id' => $medicine_id[$count],
                    'sale_quantity' => $sale_quantity[$count],
                    'purchase_batch_no' => $purchase_batch_no[$count],
                    'sale_total_amount' => $sale_total_amount[$count],
                    'unit_price' => $unit_price[$count],


                    'created_at' => now(),
                    'updated_at' => now()
                );
                $insert_data[] = $data_sale_details;
            }


            SaleDetail::insert($insert_data);


            return response()->json([
                'success' => 'Data Added successfully . ',
                'sale_id' => $sale_id
            ]);
        }
    }

    public function showPosData($id)
    {
        $all_data = SaleDetail::query()->where('sale_id', $id)->get();
        return view('admin.pos.pos_details', compact('all_data'));
    }


    public function deletePosData($id)

    {
        Sale::findorfail($id)->delete();

        return back()->with('success', 'Item Deleted successfully!');
    }

    public function editPosInfoData($id)
    {
        $repository = $this->repository;
        $pos = Sale::findorfail($id);
        return view('admin.pos.pos_info_edit', compact('pos', 'repository'));
    }

    public function editProductData($id)
    {
        $pos_product_details = SaleDetail::findorfail($id);
        return view('admin.pos.pos_product_edit', compact('pos_product_details'));
    }

    public function posInfoUpdateData(Request $request, $id)
    {
        $data = array(
            'sale_paid_amount' => $request->sale_paid_amount,
            'sale_invoice_discount' => $request->sale_discount,
            'sale_notes' => $request->sale_notes

        );
        Sale::FindorFail($request->pos_id)->update($data);
        return redirect()->route('pos.index')->with('success', 'Item updated successfully!');

    }

    public function posInvoiceData($id)
    {
        $settings = Setting::all();
        $sale_data = Sale::findorfail($id);
        $sale_details = SaleDetail::query()->where('sale_id', $id)->get();
        return view('admin.pos.pos_invoice', compact('sale_data', 'sale_details', 'settings'));

    }

    public function updateProductData(Request $request,$id)
    {
        $data = array(
            'sale_quantity' => $request->sale_quantity,
            'sale_total_amount' => $request->sale_total_amount,
        );

        SaleDetail::findorFail($id)->update($data);

        return redirect()->route('pos.index')->with('success', 'Item updated successfully!');

    }


}


