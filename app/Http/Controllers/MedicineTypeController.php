<?php

namespace App\Http\Controllers;

use App\MedicineType;
use Illuminate\Http\Request;


class MedicineTypeController extends Controller
{
    private $repository;

    public function __construct(PharmacyRepository $repository)
    {
        $this->repository = $repository;

    }



    function medicineTypeApi()
    {
        $types = MedicineType::select('id', 'medicine_type');
        return Datatables::of($types)
            ->addColumn('action', function ($types) {
                return '<a href="#" class="btn btn-xs btn-primary edit" id="' . $types->id . '">
    <i class="glyphicon glyphicon-edit"></i> Edit</a>
    <a href="#" class="btn btn-xs btn-danger delete" id="' . $types->id . '">
    <i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
            ->addColumn('checkbox', '<input type="checkbox" name="medicineType_checkbox[]" class="medicineType_checkbox"         value="{{$id}}" />')
            ->rawColumns(['checkbox', 'action'])
            ->make(true);
    }

    function postData(Request $request)
    {

        if ($request->get('button_action') == "insert") {
            $type = new MedicineType([
                'medicine_type' => $request->get('medicine_type'),

            ]);
            $type->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Created'
            ]);
        }
        if ($request->get('button_action') == 'update') {
            $type = MedicineType::find($request->get('medicineType_id'));
            $type->medicine_type = $request->get('medicine_type');
            $type->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Updated'
            ]);
        }


    }

    function fetchData(Request $request)
    {
        $id = $request->input('id');
        $type = MedicineType::find($id);
        $output = array(
            'name' => $type->name,

        );
        echo json_encode($output);
    }

    function removeData(Request $request)
    {
        $type = MedicineType::find($request->input('id'));
        if ($type->delete()) {
            echo 'Data Deleted';
        }
    }

    function massRemove(Request $request)
    {
        $medicine_id_array = $request->input('id');
        $type = MedicineType::whereIn('id', $medicine_id_array);
        if ($type->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Data Deleted'
            ]);
        }
    }
}
