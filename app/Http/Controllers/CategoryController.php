<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use DataTables;

class CategoryController extends Controller
{
    function index()
    {

        return view('admin.category.index');

    }

    function categoryApi()
    {
        $categories = Category::select('id', 'name', 'description');
        return Datatables::of($categories)
            ->addColumn('action', function ($categories) {
                return '<a href="#" class="btn btn-xs btn-primary edit" id="' . $categories->id . '">
    <i class="glyphicon glyphicon-edit"></i> Edit</a>
    <a href="#" class="btn btn-xs btn-danger delete" id="' . $categories->id . '">
    <i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
            ->addColumn('checkbox', '<input type="checkbox" name="category_checkbox[]" class="category_checkbox"         value="{{$id}}" />')
            ->rawColumns(['checkbox', 'action'])
            ->make(true);
    }

    function postData(Request $request)
    {

        if ($request->get('button_action') == "insert") {
            $category = new Category([
                'name' => $request->get('name'),
                'description' => $request->get('description')
            ]);
            $category->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Created'
            ]);
        }
        if ($request->get('button_action') == 'update') {
            $category = Category::find($request->get('category_id'));
            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Updated'
            ]);
        }


    }

    function fetchData(Request $request)
    {
        $id = $request->input('id');
        $category = Category::find($id);
        $output = array(
            'name' => $category->name,
            'description' => $category->description
        );
        echo json_encode($output);
    }

    function removeData(Request $request)
    {
        $category = Category::find($request->input('id'));
        if ($category->delete()) {
            echo 'Data Deleted';
        }
    }

    function massRemove(Request $request)
    {
        $category_id_array = $request->input('id');
        $category = Category::whereIn('id', $category_id_array);
        if ($category->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Data Deleted'
            ]);
        }
    }
}
