<?php

namespace App\Http\Controllers;

use App\Medicine;
use App\Purchase;
use App\PurchaseDetail;
use App\PurchaseInvoice;
use App\Repositories\PharmacyRepository;
use App\Supplier;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Validator;
use Gate;

class PurchaseController extends Controller
{
    private $repository;

    public function __construct(PharmacyRepository $repository)
    {


        $this->repository = $repository;
    }

    public function index()

    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }

        $all_data = Purchase::latest()->paginate(10);
        return view('admin.purchase.index', compact('all_data'));

    }

    public function createData(Request $request)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }
        $repository = $this->repository;
        return view('admin.purchase.create', compact('repository'));

    }

    public function getMedicineName(Request $request)
    {
        $supplier_id = $request->id;


        $data = Medicine::query()
            ->where('supplier_id', $supplier_id)
            ->pluck('medicine_name', 'id');

        return $data;

    }

    public function getMedicineData(Request $request)
    {
        $medicine_id = $request->id;


        $data = Medicine::select('supplier_price', 'medicine_type', 'unit', 'box_size', 'selling_price')->where('id', $medicine_id)->get();


        return response()->json($data);
    }

    public function storeData(Request $request)
    {

        if ($request->ajax()) {
            $rules = array(

                'supplier_id' => 'required',
                'purchase_date' => 'required',
                'purchase_invoice_no' => 'required',
                'medicine_id.*' => 'required',
                'unit_price.*' => 'required',
                'purchase_quantity.*' => 'required',
                'expire_date.*' => 'required',
                'purchase_batch_no.*' => 'required',
                'purchase_paid_amount' => 'required',


            );
            $error = Validator::make($request->all(), $rules);

            if ($error->fails()) {
                return response()->json([
                    'error' => $error->errors()->all()
                ]);
            }


            $data_purchase = new Purchase();

            $data_purchase->supplier_id = $request->supplier_id;
            $data_purchase->purchase_date = $request->purchase_date;
            $data_purchase->purchase_invoice_no = $request->purchase_invoice_no;
            $data_purchase->purchase_notes = $request->purchase_notes;
            $data_purchase->purchase_paid_amount = $request->purchase_paid_amount;
            $data_purchase->save();
            $purchase_id = $data_purchase->id;

            //loop data
            $medicine_id = $request->medicine_id;
            $supplier_price = $request->supplier_price;
            $purchase_batch_no = $request->purchase_batch_no;
            $expire_date = $request->expire_date;
            $box_size = $request->box_size;
            $medicine_type = $request->medicine_type;
            $purchase_unit = $request->purchase_unit;
            $unit_price = $request->unit_price;
            $purchase_quantity = $request->purchase_quantity;
            $purchase_total = $request->purchase_total;


            for ($count = 0; $count < count($medicine_id); $count++) {
                $data = array(
                    'purchase_id' => $purchase_id,
                    'medicine_id' => $medicine_id[$count],
                    'supplier_price' => $supplier_price[$count],
                    'purchase_batch_no' => $purchase_batch_no[$count],
                    'expire_date' => $expire_date[$count],
                    'box_size' => $box_size[$count],
                    'medicine_type' => $medicine_type[$count],
                    'purchase_unit' => $purchase_unit[$count],
                    'unit_price' => $unit_price[$count],
                    'purchase_quantity' => $purchase_quantity[$count],
                    'purchase_total' => $purchase_total[$count],


                    'created_at' => now(),
                    'updated_at' => now()
                );
                $insert_data[] = $data;
            }


            PurchaseDetail::insert($insert_data);


            return response()->json([
                'success' => 'Data Added successfully . '
            ]);
        }
    }

    public function showData($id)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }
        $all_data = PurchaseDetail::query()->where('purchase_id', $id)->get();
        return view('admin.purchase.purchase_details', compact('all_data'));
    }

    public function editData($id)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }
        $purchase_details = PurchaseDetail::findorfail($id);
        return view('admin.purchase.purchase_details_edit', compact('purchase_details'));
    }

    public function editPurchaseInfoData($id)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }
        $repository = $this->repository;
        $purchase = Purchase::findorfail($id);
        return view('admin.purchase.purchase_info_edit', compact('purchase', 'repository'));
    }

    public function purchaseInfoUpdateData(Request $request, $id)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }

        $this->validate($request, [
            'supplier_id' => 'required',
            'purchase_date' => 'required',
            'purchase_invoice_no' => 'required',
            'purchase_paid_amount' => 'required',


        ]);

        $purchase_table_data = array(
            'supplier_id' => $request->supplier_id,
            'purchase_date' => $request->purchase_date,
            'purchase_invoice_no' => $request->purchase_invoice_no,
            'purchase_paid_amount' => $request->purchase_paid_amount,

        );


        Purchase::findorFail($id)->update($purchase_table_data);


        return back()->with('success', 'Item Updated successfully!');

    }

    public function updateData(Request $request, $id)
    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }



        $data = array(
            'purchase_batch_no' => $request->purchase_batch_no,
            'expire_date' => $request->expire_date,
            'purchase_quantity' => $request->purchase_quantity,
            'purchase_total' => $request->purchase_total,
        );

        PurchaseDetail::findorFail($id)->update($data);

        return back()->with('success', 'Item Updated successfully!');
    }

    public function deletePurchaseData($id)

    {

        Purchase::findorfail($id)->delete();

        return back()->with('success', 'Item Deleted successfully!');
    }

    public function deletePurchaseProductData($id)

    {
        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You Can do this actjion");
        }

        PurchaseDetail::findorfail($id)->delete();

        return back()->with('success', 'Item Deleted successfully!');
    }

}
