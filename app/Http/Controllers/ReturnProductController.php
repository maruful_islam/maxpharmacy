<?php

namespace App\Http\Controllers;

use App\ReturnProduct;
use App\Sale;
use App\SaleDetail;
use Illuminate\Http\Request;

class ReturnProductController extends Controller
{
    public function index()
    {
        $sales = Sale::all();
        return view('admin.return.index', compact('sales'));
    }


    public function returnSoldProduct($id)
    {
        $sale = Sale::findOrFail($id);
        $saleDetails = SaleDetail::query()->where('sale_id', $id)->get();
        return view('admin.return.form', compact('saleDetails', 'sale'));
    }


    public function storeReturnProduct(Request $request)
    {

        for ($i = 0; $i < count($request->medicine_id); $i++) {
            if ($request['return_quantity'][$i] != null) {
                $data = new ReturnProduct;
                $data->sales_id = $request->sales_id;
                $data->medicine_id = $request['medicine_id'][$i];
                $data->return_quantity = $request['return_quantity'][$i];
                $data->return_date = $request['return_date'];
                $data->purchase_batch_no = $request['purchase_batch_no'][$i];
                $data->expire_date = $request['expire_date'][$i];
                $data->unit_price = $request['unit_price'][$i];
                $data->total_amount = $request['total_amount'][$i];
//                $data->save();
                $sold_quantity = SaleDetail::query()->where('sale_id', $request->sales_id)->first();
                $sold_quantity = $sold_quantity->sale_quantity;
                $return_quantity = $request->return_quantity[$i];
                $stock = $sold_quantity - $return_quantity;
                $prev_amount = SaleDetail::query()->where('sale_id', $request->sales_id)->first();
                $prev_amount = $prev_amount->sale_total_amount;
                $return_amount = $request->total_amount[$i];
                $curr_total = $prev_amount - $return_amount;
                $data = array(
                    'sale_total_amount' => $curr_total,
                    'sale_quantity' => $stock,
                );
                SaleDetail::query()->where('sale_id', $request->sales_id)->update($data);

                $paid_amount = array(
                    'sale_paid_amount' => $request->paid_amount
                );

                Sale::findorfail($request->sales_id)->update($paid_amount);

            }

        }
        return back()->with('success', 'Product Returned successfully!');

    }

    public function returnList()
    {
        $all_data = ReturnProduct::all();
        return view('admin.return.return_list', compact('all_data'));
    }


}
