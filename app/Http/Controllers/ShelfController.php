<?php

namespace App\Http\Controllers;

use App\Shelf;
use Illuminate\Http\Request;
//use App\DynamicField;
use Validator;
use DataTables;


class ShelfController extends Controller
{
    public function index()
    {
        return view('admin.shelf.index');
    }

    function shelfApi()
    {
        $shelves = Shelf::select('id', 'name', 'shelf_number');
        return Datatables::of($shelves)
            ->addColumn('action', function ($shelves) {
                return '<a href="#" class="btn btn-xs btn-primary edit" id="' . $shelves->id . '"><i class="glyphicon glyphicon-edit"></i> Edit</a><a href="#" class="btn btn-xs btn-danger delete" id="' . $shelves->id . '"><i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function insert(Request $request)
    {
        if ($request->ajax()) {
            $rules = array(
                'name.*' => 'required',
                'shelf_number.*' => 'required'
            );
            $error = Validator::make($request->all(), $rules);

            if ($error->fails()) {
                return response()->json([
                    'error' => $error->errors()->all()
                ]);
            }

            $name = $request->name;
            $shelf_number = $request->shelf_number;
//            dd($shelf_number);

            for ($count = 0; $count < count($name); $count++) {
                $data = array(
                    'name' => $name[$count],
                    'shelf_number' => $shelf_number[$count],
                    'created_at' => now(),
                    'updated_at' => now()
                );
                $insert_data[] = $data;
            }

            Shelf::insert($insert_data);
            return response()->json([
                'success' => 'Data Added successfully.'
            ]);

        }
    }

    function fetchData(Request $request)
    {
        $id = $request->input('id');
        $shelf = Shelf::find($id);
        $output = array(
            'name' => $shelf->name,
            'shelf_number' => $shelf->shelf_number
        );
        echo json_encode($output);
    }

    function postData(Request $request)
    {

        if ($request->get('button_action') == 'update') {
            $shelf = Shelf::find($request->get('shelf_id'));
            $shelf->name = $request->get('name');
            $shelf->shelf_number = $request->get('shelf_number');
            $shelf->save();

            return response()->json([
                'success' => true,
                'message' => 'Data Updated'
            ]);

        }


    }

    function removeData(Request $request)
    {
        $shelf = Shelf::find($request->input('id'));
        if ($shelf->delete()) {
            echo 'Data Deleted';
        }
    }
}
