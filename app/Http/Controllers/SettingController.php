<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::all();
        return view('admin.settings.index', compact('setting'));
    }


    public function editData($id)
    {
        $setting = Setting::findorfail($id);
        return view('admin.settings.edit', compact('setting'));
    }

    public function updateData(Request $request, $id)
    {
        Setting::findorfail($id)->update($request->all());
        return back()->with('success', 'Setting Updated successfully!');
    }

}
