<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    public function index()
    {
        $customer_data = Customer::latest()->paginate(10);
        return view('admin.customer.index', compact('customer_data'));

    }

    public function storeData(Request $request)
    {
        $this->validate($request, [
            'customer_name' => 'required',
            'email' => 'required',
            'customer_phone_number' => 'required',
            'details' => 'required',
        ]);

        Customer::create($request->all());

        return back()->with('success', 'Item created successfully!');


    }

    public function editData($id)
    {
        $customer = Customer::find($id);
        $customer_data = Customer::latest()->paginate(5);
        return view('admin.customer.edit', compact('customer', 'customer_data'));

    }

    public function updateData(Request $request, $id)
    {
        $this->validate($request, [
            'customer_name' => 'required',
            'email' => 'required',
            'customer_phone_number' => 'required',
            'details' => 'required',
        ]);
        // id ta age deta hobe
        Customer::findorFail($id)->update($request->all());

        return redirect()->route('customer.index')
            ->with('success', 'Data updated successfully');
    }

    public function deleteData($id)
    {
        Customer::find($id)->delete();

        return redirect()->route('customer.index')
            ->with('success', 'Data deleted successfully');
    }


    function searchData(Request $request)
    {

        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            if ($query != '') {
                $data = Customer::query()
                    ->where('customer_name', 'like', '%' . $query . '%')
                    ->orwhere('email', 'like', '%' . $query . '%')
                    ->orwhere('customer_phone_number', 'like', '%' . $query . '%')
                    ->orwhere('details', 'like', '%' . $query . '%')
                    ->orderBy('id', 'desc')
                    ->limit(10)
                    ->get();
            } else {
                $data = Customer::query()
                    ->orderBy('id', 'desc')
                    ->limit(10)
                    ->get();
            }
            $total_row = $data->count();
            if ($total_row > 0) {
                foreach ($data as $row) {
                    $output .= '
                    <tr>
                     <td>' . $row->id . '</td>
                     <td>' . $row->customer_name . '</td>
                     <td>' . $row->email . '</td>
                     <td>' . $row->customer_phone_number . '</td>
                     <td>' . $row->details . '</td>
                     <td><a class="btn btn-primary" href=' . url('customer/' . $row->id) . '>Edit</a>
                     <a class="btn btn-primary" href=' . url('deletecustomer/' . $row->id) . '> Delete</a>
                    
                     </td>
                    </tr>';
                }
            } else {
                $output = '
                   <tr>
                    <td align="center" colspan="6">No Data Found</td>
                   </tr>
                   ';
            }


            //echo json_encode($data);
            return $output;
        }
    }

}
