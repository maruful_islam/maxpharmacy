<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SupplierController extends Controller
{
    public function index()
    {

        return view('admin.supplier.index');
    }


    public function store(Request $request)
    {

        $input = $request->all();
        //photo process
        $input['logo'] = null;
        if ($request->hasFile('logo')) {
            $input['logo'] = Date('Y') . "_" . substr(md5(time()), 0, 6) . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('/upload/photo/'), $input['logo']);
        }

        Supplier::create($input);

        return response()->json([
            'success' => true,
            'message' => 'Contact Created'
        ]);

    }

    public function edit($id)
    {

        $supplier_id = Supplier::findOrFail($id);
        return $supplier_id;
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $supplier_id = Supplier::findOrFail($id);
        // file processing
        $input['logo'] = $supplier_id->logo;

        if ($request->hasFile('logo')) {
            if (!$supplier_id->logo == NULL) {
                unlink(public_path('upload/photo/' . $supplier_id->logo));
            }
            $input['logo'] = Date('Y') . "_" . substr(md5(time()), 0, 6) . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('/upload/photo/'), $input['logo']);
        }
        //end file processing

        $supplier_id->update($input);

        return response()->json([
            'success' => true,
            'message' => 'Contact Updated'
        ]);
    }

    public function destroy($id)
    {

        $supplier_id = Supplier::findOrFail($id);

//        var_dump($id);
//        die;

        if (!$supplier_id->logo == NULL) {
            unlink(public_path('upload/photo/' . $supplier_id->logo));
        }

    Supplier::destroy($id);

        return response()->json([
            'success' => true,
            'message' => 'Contact Deleted'
        ]);


    }


    public function supplierApi()
    {
        $supplierData = Supplier::all();

        return Datatables::of($supplierData)
            ->addColumn('logo', function ($supplierData) {
                if ($supplierData->logo == NULL) {
                    return 'No Image';
                }
                return '<img class="rounded-square" width="50" height="50" src="' . asset("upload/photo/" . $supplierData->logo) . '" alt="">';
            })
            ->addColumn('action', function ($supplierData) {
                return
                    '<a onclick="editData(' . $supplierData->id . ')"  class="btn btn-xs  btn-outline-secondary supplier"><i
                                                class="fas fa-pen"></i> </a> ' .
                    '<a onclick="deleteData(' . $supplierData->id . ')"  class="btn btn-xs  btn-outline-secondary supplier_delete"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['logo', 'action'])->make(true);
    }


}

