<?php

namespace App\Http\Controllers;

use App\MedicineUnit;
use Illuminate\Http\Request;


class MedicineUnitController extends Controller
{
    private $repository;


    function index()
    {

        return view('admin.type.index');

    }

    function medicineUnitApi()
    {
        $units = MedicineUnit::select('id', 'medicine_unit');
        return Datatables::of($units)
            ->addColumn('action', function ($units) {
                return '<a href="#" class="btn btn-xs btn-primary edit" id="' . $units->id . '">
    <i class="glyphicon glyphicon-edit"></i> Edit</a>
    <a href="#" class="btn btn-xs btn-danger delete" id="' . $units->id . '">
    <i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
            ->rawColumns('action')
            ->make(true);
    }

    function postData(Request $request)
    {

        if ($request->get('button_action') == "insert") {
            $unit = new MedicineUnit([
                'medicine_unit' => $request->get('medicine_unit'),

            ]);
            $unit->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Created'
            ]);
        }
        if ($request->get('button_action') == 'update') {
            $unit = MedicineUnit::find($request->get('medicineUnit_id'));
            $unit->medicine_unit = $request->get('medicine_unit');
            $unit->save();
            return response()->json([
                'success' => true,
                'message' => 'Data Updated'
            ]);
        }


    }

    function fetchData(Request $request)
    {
        $id = $request->input('id');
        $unit = MedicineUnit::find($id);
        $output = array(
            'medicine_unit' => $unit->medicine_unit,

        );
        echo json_encode($output);
    }

    function removeData(Request $request)
    {
        $unit = MedicineUnit::find($request->input('id'));
        if ($unit->delete()) {
            echo 'Data Deleted';
        }
    }
//
//    function massRemove(Request $request)
//    {
//        $medicine_id_array = $request->input('id');
//        $type = MedicineType::whereIn('id', $medicine_id_array);
//        if ($type->delete()) {
//            return response()->json([
//                'success' => true,
//                'message' => 'Data Deleted'
//            ]);
//        }
//    }
}
