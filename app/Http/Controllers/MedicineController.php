<?php

namespace App\Http\Controllers;

use App\Category;
use App\Medicine;
use App\Shelf;
use App\Supplier;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Gate;
use App\Repositories\PharmacyRepository;


class MedicineController extends Controller
{
    private $repository;

    public function __construct(PharmacyRepository $repository)
    {
        $this->repository = $repository;

    }

    public function index()
    {
        if (!Gate::allows('isAdmin')) {
            abort(404, "Sorry, You Can do this actjion");
        }
        $all_data = Medicine::latest()->paginate(10);
        return view('admin.medicine.index', compact('all_data'));


    }

    public function createData()
    {
        if (!Gate::allows('isAdmin')) {
            abort(404, "Sorry, You Can do this actjion");
        }
        $supplier_data = Supplier::pluck('name', 'id');
        $shelf_data = Shelf::pluck('shelf_number', 'id');
        $category_data = Category::pluck('name', 'id');
        $repository = $this->repository;
        return view('admin.medicine.create', compact('supplier_data', 'shelf_data', 'category_data', 'repository'));
    }

    public function storeData(Request $request)
    {
        $this->validate($request, [
            'medicine_name' => 'required',
            'medicine_type' => 'required',
            'medicine_details' => 'required',
            'selling_price' => 'required',
            'generic_name' => 'required',
            'unit' => 'required',
            'medicine_image' => 'required',
            'box_size' => 'required',
            'supplier_price' => 'required',
            'supplier_id' => 'required',
            'category_id' => 'required',
            'shelf_id' => 'required',
        ]);

        $image = $request->file('medicine_image');
        $image_name = substr(md5(time()), 0, 6);
        $filename = Date('Y') . '_' . $image_name . "." . $image->getClientOriginalExtension();
        $image->move(base_path('public/admin/upload/'), $filename);

        $data = $request->except('medicine_image');
        $data['medicine_image'] = $filename;
        Medicine::create($data);

        return back()->with('success', 'Item created successfully!');
    }

    public function editData($id)
    {

        if (!Gate::allows('isAdmin')) {
            abort(404, "Sorry, You Can do this actjion");
        }

        $supplier_data = Supplier::pluck('name', 'id');
        $shelf_data = Shelf::pluck('name', 'id');
        $category_data = Category::pluck('name', 'id');
        $medicine = Medicine::find($id);
        $medicine_data = Medicine::latest()->paginate(5);
        return view('admin.medicine.edit', compact('medicine', 'medicine_data', 'supplier_data', 'shelf_data', 'category_data'));

    }

    public function updateData(Request $request, $id)
    {
        if (!Gate::allows('isAdmin')) {
            abort(404, "Sorry, You Can do this actjion");
        }


//        $this->validate($request, [
//            'medicine_name' => 'required',
//            'medicine_type' => 'required',
//            'medicine_details' => 'required',
//            'selling_price' => 'required',
//            'generic_name' => 'required',
//            'unit' => 'required',
//            'expire_date' => 'required',
//            'batch_number' => 'required',
//            'box_size' => 'required',
//            'supplier_price' => 'required',
//            'supplier_id' => 'required',
//            'category_id' => 'required',
//            'shelf_id' => 'required',
//        ]);
        $data = $request->except('medicine_image');

        $medicine_id = Medicine::findorFail($id);

        if ($request->hasFile('medicine_image')) {

            if (!$medicine_id->medicine_image == NULL) {
                unlink(public_path('admin/upload/' . $medicine_id->medicine_image));
            }
            $image = $request->file('medicine_image');
            $image_name = substr(md5(time()), 0, 6);
            $filename = Date('Y') . '_' . $image_name . "." . $image->getClientOriginalExtension();
            $image->move(base_path('public/admin/upload/'), $filename);
            $data['medicine_image'] = $filename;

        } else {

            $data['medicine_image'] = $medicine_id->medicine_image;
        }
//            dd($data);

        Medicine::findorFail($id)->update($data);

        return back()->with('success', 'Item Updated successfully!');
    }

    public function deleteData($id)
    {
        if (!Gate::allows('isAdmin')) {
            abort(404, "Sorry, You Can do this actjion");
        }
        Medicine::find($id)->delete();

        return redirect()->route('medicine.index')
            ->with('success', 'Data deleted successfully');
    }

}
